/*
 * Note: performance bottleneck: matrix operation, floating point arithmetic
 */

#include <string.h>
#include <jni.h>
#include <stdio.h>
#include <android/log.h>
#include <time.h>

// Openfst
#include <fst/fstlib.h>

#include "ginkgo-nnet-decoder.h"

// HJP:ADD:2013.08.17
#include "base/kaldi-common.h"
//#include "nnet/nnet-nnet.h"
// approximate time profiling
//#include <time.h>
//clock_t profile_t = 0;

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jstring 	JNICALL Java_org_shrc_nativeinterface_NativeInterface_test(JNIEnv* env, jobject javaThis, jstring msg);

JNIEXPORT jboolean 	JNICALL Java_org_shrc_nativeinterface_NativeInterface_init(JNIEnv* env, jobject javaThis);
JNIEXPORT jstring 	JNICALL Java_org_shrc_nativeinterface_NativeInterface_decode(JNIEnv* env, jobject javaThis);
JNIEXPORT jboolean 	JNICALL Java_org_shrc_nativeinterface_NativeInterface_release(JNIEnv* env, jobject javaThis);

#ifdef __cplusplus
}
#endif

extern int compute_fbank_feats(int argc, char *argv[]);
extern int compute_spectrogram_feats(int argc, char *argv[]);

JNIEXPORT jstring JNICALL Java_org_shrc_nativeinterface_NativeInterface_test(JNIEnv* env, jobject javaThis, jstring msg) {

	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Entered Test");
	KALDI_LOG << "Entered Test";

////	char *arg_fbank[] = {"compute-fbank-feats", "scp:/sdcard/shrc/wav.scp", "ark,t:/sdcard/shrc/fbank.ark"};
////	compute_fbank_feats(sizeof(arg_fbank) / sizeof(char *), arg_fbank);
//	char *arg_spec[] = {"compute-spectrogram-feats", "scp:/sdcard/shrc/wav.scp", "ark,t:/sdcard/shrc/spectrum.ark"};
//	compute_spectrogram_feats(sizeof(arg_spec) / sizeof(char *), arg_spec);
//
//	// Open FST Test
//	fst::StdVectorFst fst;
//	// Adds state 0 to the initially empty FST and make it the start state.
//	fst.AddState();   // 1st state will be state 0 (returned by AddState)
//	fst.SetStart(0);  // arg is state ID
//
//	// Adds two arcs exiting state 0.
//	// Arc constructor args: ilabel, olabel, weight, dest state ID.
//	fst.AddArc(0, fst::StdArc(1, 1, 0.5, 1));  // 1st arg is src state ID
//	fst.AddArc(0, fst::StdArc(2, 2, 1.5, 1));
//
//	// Adds state 1 and its arc.
//	fst.AddState();
//	fst.AddArc(1, fst::StdArc(3, 3, 2.5, 2));
//
//	// Adds state 2 and set its final weight.
//	fst.AddState();
//	fst.SetFinal(2, 3.5);  // 1st arg is state ID, 2nd arg weight
	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Leaving Test");

	KALDI_LOG << "Leaving Test from Kaldi_log";

//	Java_org_shrc_nativeinterface_NativeInterface_init(env, javaThis);
//	Java_org_shrc_nativeinterface_NativeInterface_decode(env, javaThis);
//	Java_org_shrc_nativeinterface_NativeInterface_release(env, javaThis);

	return env->NewStringUTF("Hello World of NDK !");
}

static pku::shrc::ginkgo::GinkgoNnet* ginkgo_nnet = NULL;

JNIEXPORT jboolean JNICALL Java_org_shrc_nativeinterface_NativeInterface_init(JNIEnv* env, jobject javaThis) {
	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Entered init");
	KALDI_LOG << "Entered init";
	char *argv[] = {
			"jinkgo-nnet-decoder",
			"--allow-partial=true",
			"--max-active=4000",
			"--beam=10.0",
			"--acoustic-scale=0.0666",
			"--word-symbol-table=/sdcard/shrc/words_utf.txt",
			"--feature-transform=/sdcard/shrc/final.feature_transform",
			"--class-frame-counts=/sdcard/shrc/ali_train_pdf.counts",
			"--nnet-model=/sdcard/shrc/final.nnet",
			"--no-softmax=true",
			"/sdcard/shrc/final.mdl",
			"/sdcard/shrc/HCLG.fst"};
	int argc = sizeof(argv) / sizeof(char*);
	ginkgo_nnet = new pku::shrc::ginkgo::GinkgoNnet(argc,argv);
//	__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Leaving init");
	KALDI_LOG << "Leaving init";
	return true;
}

JNIEXPORT jstring JNICALL Java_org_shrc_nativeinterface_NativeInterface_decode(JNIEnv* env, jobject javaThis) {
//	__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Entered decode");
	KALDI_LOG << "Entered decode";

//	profile_t = 0;
//
	char *arg_fbank[] = {
			"compute-fbank-feats",
			"--config=/sdcard/shrc/fbank.conf",
			"scp:/sdcard/shrc/record.scp",
			//			"scp:/sdcard/shrc/test.scp",	// used for only test, ignore recorded wav
			"ark:/sdcard/shrc/fbank.ark" };
	compute_fbank_feats(sizeof(arg_fbank) / sizeof(char *), arg_fbank);
	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, "features retrieved");
	KALDI_LOG << "feature retrieved";

	//kaldi::MatrixBase<kaldi::BaseFloat>::profile_time.tv_sec = kaldi::MatrixBase<kaldi::BaseFloat>::profile_time.tv_usec = 0;
#ifdef ANDROID
	kaldi::MatrixBase<kaldi::BaseFloat>::profile_time = {0, 0};
#endif

	char *feature_path = "/sdcard/shrc/fbank.ark";
	string result = ginkgo_nnet->DecodeUtt(feature_path);
	//result = "result: " + result;
	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, result.c_str());
#ifdef ANDROID
	KALDI_LOG << "result: " << result.c_str()
			<< "MatrixBase.addMatMat:time: " <<
			kaldi::Timer::convertToDouble(kaldi::MatrixBase<kaldi::BaseFloat>::profile_time);
#endif

	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Leaving decode");

	jstring r = env->NewStringUTF(result.c_str());
	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Leaving decode");
	KALDI_LOG << "Leaving decode";
	return r;

//	KALDI_LOG << "Leaving decode";
//	return env->NewStringUTF("Decode Result Stub");
}

JNIEXPORT jboolean 	JNICALL Java_org_shrc_nativeinterface_NativeInterface_release(JNIEnv* env, jobject javaThis) {
	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Entered release");
	KALDI_LOG << "Entered release";
	delete ginkgo_nnet;
	ginkgo_nnet = NULL;
	//__android_log_write(ANDROID_LOG_VERBOSE, TAG, "Leaving release");
	KALDI_LOG << "Leaving release";
	return true;
}

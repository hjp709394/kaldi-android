// bin/decode-faster-mapped.cc

// Copyright 2009-2011  Microsoft Corporation

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.
#ifndef PKU_SHRC_GINKGO_LIB_GINKGO_NNET_DECODER_H
#define PKU_SHRC_GINKGO_LIB_GINKGO_NNET_DECODER_H

#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "tree/context-dep.h"
#include "hmm/transition-model.h"
#include "fstext/fstext-lib.h"
#include "decoder/faster-decoder.h"
#include "decoder/decodable-matrix.h"
#include "util/timer.h"
#include "nnet/nnet-nnet.h"
#include "nnet/nnet-loss.h"
#include "lat/kaldi-lattice.h" // for {Compact}LatticeArc

namespace pku { namespace shrc { namespace ginkgo{

	using fst::SymbolTable;
	using fst::VectorFst;
	using fst::StdArc;
	struct NnetForwardOptions {

		std::string feature_transform;
		std::string class_frame_counts;
		std::string nnet_model;
		bool apply_log;
		bool no_softmax;
		bool htk_in;
		bool loglikes_in;
		kaldi::BaseFloat prior_scale;

		NnetForwardOptions(): apply_log(false),
			no_softmax(false), htk_in(false), 
			loglikes_in(false), prior_scale(1.0){}

		void Register(kaldi::ParseOptions *po)
		{
			po->Register("feature-transform", &feature_transform, "Feature transform Neural Network");
			po->Register("class-frame-counts", &class_frame_counts, "Counts of frames for posterior division by class-priors");
			po->Register("apply-log", &apply_log, "Transform MLP output to logscale");
			po->Register("no-softmax", &no_softmax, "No softmax on MLP output. The MLP outputs directly log-likelihoods, log-priors will be subtracted");
			po->Register("nnet-model", &nnet_model, "nnet models");
			po->Register("htk-in", &htk_in, "Read input as HTK features");
			po->Register("loglikes-in", &loglikes_in, "Read input as loglikes");
			po->Register("prior-scale", &prior_scale, "scaling factor of prior log-probabilites given by --class-frame-counts");
		}
	};
	class GinkgoNnetHelper {
	public:
		static void InitNnet(NnetForwardOptions &nnet_opts, 
			kaldi::Nnet &nnet_transf, 
			kaldi::Nnet &nnet, 
			kaldi::CuVector<kaldi::BaseFloat> &priors);

		static void NnetForward(NnetForwardOptions &nnet_opts, 
			kaldi::Nnet &nnet_transf, 
			kaldi::Nnet &nnet, 
			kaldi::CuVector<kaldi::BaseFloat> &priors,
			std::string key,
			const kaldi::MatrixBase<kaldi::BaseFloat> &mat,
			kaldi::Matrix<kaldi::BaseFloat> &nnet_out_host);


		static bool Decode(std::string key, 
			kaldi::Matrix<kaldi::BaseFloat> &loglikes,
			kaldi::FasterDecoder &decoder,
			kaldi::TransitionModel &trans_model,
			fst::SymbolTable *word_syms,
			kaldi::BaseFloat acoustic_scale,
			bool allow_partial,
			kaldi::int64 &num_fail,
			kaldi::int64 &num_success,
			kaldi::int64 &frame_count,
			kaldi::BaseFloat &tot_like,
			bool silent,
			std::vector<string> &rec_result);
	};
	class GinkgoNnet {
	public:
		GinkgoNnet(int argc, char *argv[]);
		~GinkgoNnet();
		int Initialize(int argc, char *argv[]);
		std::string DecodeUtt(const char * featrespter,kaldi::BaseFloat acoustic_scale);
		std::string DecodeUtt(const char * featrespter);

	public:
		bool binary;
		kaldi::BaseFloat acoustic_scale;
		bool allow_partial;
		bool silent;
		kaldi::FasterDecoderOptions decoder_opts;	
		NnetForwardOptions nnet_opts;
		std::string word_syms_filename;
		std::string senone_syms_filename;
		std::string model_in_filename;
		std::string fst_in_filename;
		std::string feature_rspecifier;
		std::string words_wspecifier;
		std::string alignment_wspecifier;
		std::string loglikes_rspecifier;
		kaldi::TransitionModel trans_model;
		fst::SymbolTable *word_syms;
		fst::SymbolTable *senone_syms;
		VectorFst<StdArc> *decode_fst;
		kaldi::Nnet nnet_transf;
		kaldi::Nnet nnet;
		kaldi::CuVector<kaldi::BaseFloat> priors;
		kaldi::FasterDecoder * pdecoder;
	};
}}}
#endif

APP_PLATFORM := android-17

LOCAL_ARM_MODE := arm
APP_ABI := armeabi-v7a
TARGET_CPU_ABI := armeabi-v7a

APP_CFLAGS += -fexceptions -frtti -O3 -DANDROID -mfloat-abi=softfp -mfpu=neon
#APP_CPPFLAGS += -fexceptions -frtti -DANDROID

# Not to use the default values, which has -g for debugging
#APP_CFLAGS = -fexceptions -frtti -DANDROID -o3
#APP_CPPFLAGS = -fexceptions -frtti -DANDROID -o3

APP_STL := gnustl_static
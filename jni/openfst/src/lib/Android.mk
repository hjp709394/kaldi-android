include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/openfst/src/lib
LOCAL_C_INCLUDES += $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := fst
LOCAL_SRC_FILES := compat.cc fst.cc symbol-table.cc util.cc flags.cc properties.cc symbol-table-ops.cc
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

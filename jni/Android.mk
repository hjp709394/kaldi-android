LOCAL_PATH := $(call my-dir)
TOP_PATH := $(LOCAL_PATH)
#APP_OPTIM := release
#APP_OPTIM := debug

##################### JNI Interface lib #####################

include $(CLEAR_VARS)

# ndk performs a debug checking even for avoiding runtime linking error 
# and for some motivations it doesn't find the correct references
# in a pre-built library
LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

LOCAL_PATH := $(TOP_PATH)
LOCAL_MODULE    := decoder
LOCAL_C_INCLUDES += $(LOCAL_PATH)/openfst/src/include/ $(LOCAL_PATH)/kaldi/src
LOCAL_STATIC_LIBRARIES := kaldi-base kaldi-matrix fst kaldi-util kaldi-thread kaldi-feat kaldi-tree kaldi-gmm kaldi-tied kaldi-transform kaldi-sgmm kaldi-fstext kaldi-hmm kaldi-lm kaldi-decoder kaldi-lat kaldi-nnet kaldi-cudamatrix kaldi-nnet-cpu kaldi-sgmm2 #kaldi-optimization 
LOCAL_SRC_FILES := decoder.cpp compute-fbank-feats.cc compute-spectrogram-feats.cc ginkgo-nnet-decoder.cpp  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog #-L$(TOP_PATH)/../obj/local/$(TARGET_ARCH_ABI) -lkaldi-nnet -lkaldi-base -lkaldi-matrix -lkaldi-util -lkaldi-thread -lkaldi-feat -lkaldi-tree -lkaldi-gmm -lkaldi-tied -lkaldi-transform -lkaldi-sgmm -lkaldi-fstext -lkaldi-hmm -lkaldi-lm -lkaldi-decoder -lkaldi-lat -lfst -lkaldi-cudamatrix -lkaldi-nnet-cpu -lkaldi-sgmm2 #-lkaldi-optimization	# use LOCAL_STATIC_LIBRARIES instead of -l flags
 
include $(BUILD_SHARED_LIBRARY)

##################### Open FST lib ##########################

include $(TOP_PATH)/openfst/src/lib/Android.mk		# fst lib

##################### Kaldi lib #############################
# HJP:NOTE:2013.08.16: I donot know which kaldi lib depends on the fst lib

include $(TOP_PATH)/kaldi/src/base/Android.mk		# kaldi-base lib
include $(TOP_PATH)/kaldi/src/matrix/Android.mk		# kaldi-matrix lib
include $(TOP_PATH)/kaldi/src/cudamatrix/Android.mk		# kaldi-cudamatrix lib
include $(TOP_PATH)/kaldi/src/util/Android.mk		# kaldi-util lib
include $(TOP_PATH)/kaldi/src/thread/Android.mk		# kaldi-thread lib
include $(TOP_PATH)/kaldi/src/feat/Android.mk		# kaldi-feat lib
include $(TOP_PATH)/kaldi/src/tree/Android.mk		# kaldi-tree lib
include $(TOP_PATH)/kaldi/src/decoder/Android.mk		# kaldi-decoder lib
include $(TOP_PATH)/kaldi/src/fstext/Android.mk		# kaldi-fstext lib
include $(TOP_PATH)/kaldi/src/gmm/Android.mk		# kaldi-gmm lib
include $(TOP_PATH)/kaldi/src/hmm/Android.mk		# kaldi-hmm lib
include $(TOP_PATH)/kaldi/src/lm/Android.mk		# kaldi-lm lib
include $(TOP_PATH)/kaldi/src/lat/Android.mk		# kaldi-lat lib
include $(TOP_PATH)/kaldi/src/nnet/Android.mk		# kaldi-nnet lib
include $(TOP_PATH)/kaldi/src/nnet-cpu/Android.mk		# kaldi-nnet-cpu lib
include $(TOP_PATH)/kaldi/src/sgmm/Android.mk		# kaldi-sgmm lib
include $(TOP_PATH)/kaldi/src/tied/Android.mk		# kaldi-tied lib
include $(TOP_PATH)/kaldi/src/transform/Android.mk		# kaldi-transform lib
include $(TOP_PATH)/kaldi/src/sgmm2/Android.mk		# kaldi-sgmm2 lib
#include $(TOP_PATH)/kaldi/src/optimization/Android.mk		# kaldi-optimization lib

## Dependency list ###
##1)The tools depend on all the libraries
# bin fstbin gmmbin fgmmbin tiedbin sgmmbin sgmm2bin featbin nnetbin nnet-cpubin latbin: base matrix util feat tree optimization thread gmm tied transform sgmm sgmm2 fstext hmm lm decoder lat cudamatrix nnet nnet-cpu
# 
##2)The libraries have inter-dependencies
# base:
# matrix : base
# util: base matrix
# thread: util
# feat: base matrix util gmm transform
# tree: base util matrix
# optimization: base matrix
# gmm: base util matrix tree
# tied: base util matrix gmm tree transform
# transform: base util matrix gmm tree
# sgmm: base util matrix gmm tree transform thread hmm
# sgmm2: base util matrix gmm tree transform thread hmm
# fstext: base util matrix tree
# hmm: base tree matrix
# lm: base util
# decoder: base util matrix gmm sgmm hmm tree transform
# lat: base util hmm
# cudamatrix: base util matrix
# nnet: base util matrix cudamatrix
# nnet-cpu: base util matrix thread
# #3)Dependencies for optional parts of Kaldi
# onlinebin: base matrix util feat tree optimization gmm tied transform sgmm sgmm2 fstext hmm lm decoder lat cudamatrix nnet nnet-cpu online
# # python-kaldi-decoding: base matrix util feat tree optimization thread gmm tied transform sgmm sgmm2 fstext hmm decoder lat online
# online: decoder
# kwsbin: fstext lat base util
# 


#############################################################
#include $(CLEAR_VARS)
#LOCAL_PATH := $(TOP_PATH)/openfst/src/lib
#LOCAL_C_INCLUDES += $(TOP_PATH)/openfst/src/include/
#LOCAL_MODULE    := fst
#LOCAL_SRC_FILES := compat.cc fst.cc symbol-table.cc util.cc flags.cc properties.cc symbol-table-ops.cc
#LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
#include $(BUILD_STATIC_LIBRARY)


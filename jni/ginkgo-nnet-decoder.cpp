#include "ginkgo-nnet-decoder.h"

using namespace pku::shrc::ginkgo;

void GinkgoNnetHelper::InitNnet(NnetForwardOptions &nnet_opts,
		kaldi::Nnet &nnet_transf, kaldi::Nnet &nnet,
		kaldi::CuVector<kaldi::BaseFloat> &priors) {
	typedef kaldi::int32 int32;
	typedef kaldi::BaseFloat BaseFloat;
	//Nnet nnet_transf;
	if (nnet_opts.feature_transform != "") {
		KALDI_VLOG(2) << "Loading feature_transform: "
				<< nnet_opts.feature_transform;
		nnet_transf.Read(nnet_opts.feature_transform);
	}

	//Nnet nnet;
	KALDI_VLOG(2) << "Loading Nnet: " << nnet_opts.nnet_model;
	nnet.Read(nnet_opts.nnet_model);

	kaldi::Vector<kaldi::BaseFloat> tmp_priors;
	if (nnet_opts.class_frame_counts != "") {
		kaldi::Input in;
		in.OpenTextMode(nnet_opts.class_frame_counts);
		tmp_priors.Read(in.Stream(), false);
		in.Close();

		//create inv. priors, or log inv priors 
		BaseFloat sum = tmp_priors.Sum();
		tmp_priors.Scale(1.0 / sum);
		if (nnet_opts.apply_log || nnet_opts.no_softmax) {
			tmp_priors.ApplyLog();
			tmp_priors.Scale(-nnet_opts.prior_scale);
		} else {
			tmp_priors.ApplyPow(-nnet_opts.prior_scale);
		}

		//detect the inf, replace by something reasonable (maximal non-inf value)
		//a) replace inf by -inf
		for (int32 i = 0; i < tmp_priors.Dim(); i++) {
			if (tmp_priors(i) == std::numeric_limits<BaseFloat>::infinity()) {
				tmp_priors(i) *= -1.0;
			}
		}
		//b) find max
		BaseFloat max = tmp_priors.Max();
		//c) replace -inf by max prior
		for (int32 i = 0; i < tmp_priors.Dim(); i++) {
			if (tmp_priors(i) == -std::numeric_limits<BaseFloat>::infinity()) {
				tmp_priors(i) = max;
			}
		}

		// push priors to GPU
		priors.Resize(tmp_priors.Dim());
		priors.CopyFromVec(tmp_priors);
	}
}

void GinkgoNnetHelper::NnetForward(NnetForwardOptions &nnet_opts,
		kaldi::Nnet &nnet_transf, kaldi::Nnet &nnet,
		kaldi::CuVector<kaldi::BaseFloat> &priors, std::string key,
		const kaldi::MatrixBase<kaldi::BaseFloat> &mat,
		kaldi::Matrix<kaldi::BaseFloat> &nnet_out_host) {
	typedef kaldi::int32 int32;
	typedef kaldi::BaseFloat BaseFloat;

	// check for NaN/inf
	for (int32 r = 0; r < mat.NumRows(); r++) {
		for (int32 c = 0; c < mat.NumCols(); c++) {
			BaseFloat val = mat(r, c);
			if (val != val)
				KALDI_ERR << "NaN in features of : " << key;
			if (val == std::numeric_limits<BaseFloat>::infinity())
				KALDI_ERR << "inf in features of : " << key;
		}
	}
	// push it to gpu
	kaldi::CuMatrix<BaseFloat> feats, feats_transf, nnet_out;

	feats = mat;
	// fwd-pass
	nnet_transf.Feedforward(feats, &feats_transf);
	nnet.Feedforward(feats_transf, &nnet_out);

	// convert posteriors to log-posteriors
	if (nnet_opts.apply_log) {
		nnet_out.ApplyLog();
	}

	// divide posteriors by priors to get quasi-likelihoods
	if (nnet_opts.class_frame_counts != "") {
		if (nnet_opts.apply_log || nnet_opts.no_softmax) {
			nnet_out.AddVecToRows(1.0, priors, 1.0);
		} else {
			nnet_out.MulColsVec(priors);
		}
	}

	//download from GPU
	nnet_out_host.Resize(nnet_out.NumRows(), nnet_out.NumCols());
	nnet_out.CopyToMat(&nnet_out_host);

	//check for NaN/inf
	for (int32 r = 0; r < nnet_out_host.NumRows(); r++) {
		for (int32 c = 0; c < nnet_out_host.NumCols(); c++) {
			BaseFloat val = nnet_out_host(r, c);
			if (val != val)
				KALDI_ERR << "NaN in NNet output of : " << key;
			if (val == std::numeric_limits<BaseFloat>::infinity())
				KALDI_ERR << "inf in NNet cout put of : " << key;
		}
	}
}

bool GinkgoNnetHelper::Decode(std::string key,
		kaldi::Matrix<kaldi::BaseFloat> &loglikes,
		kaldi::FasterDecoder &decoder, kaldi::TransitionModel &trans_model,
		fst::SymbolTable *word_syms, kaldi::BaseFloat acoustic_scale,
		bool allow_partial, kaldi::int64 &num_fail, kaldi::int64 &num_success,
		kaldi::int64 &frame_count, kaldi::BaseFloat &tot_like, bool silent,
		std::vector<string> &p_rec_result) {
	using namespace kaldi;
	using fst::SymbolTable;
	using fst::VectorFst;
	using fst::StdArc;

	if (loglikes.NumRows() == 0) {
		KALDI_WARN << "Zero-length utterance: " << key;
		num_fail++;
		return true;
	}
	DecodableMatrixScaledMapped decodable(trans_model, loglikes,
			acoustic_scale);
	decoder.Decode(&decodable);

	VectorFst<LatticeArc> decoded; // linear FST.

	if ((allow_partial || decoder.ReachedFinal())
			&& decoder.GetBestPath(&decoded)) {
		num_success++;
		if (!decoder.ReachedFinal())
			KALDI_WARN
					<< "Decoder did not reach end-state, outputting partial traceback.";

		std::vector<kaldi::int32> alignment;
		std::vector<kaldi::int32> words;
		std::vector<LatticeWeight> vec_weight;
		LatticeWeight weight;
		std::vector<std::string> vec_str_words;
		frame_count += loglikes.NumRows();

		//GetLinearSymbolSequence(decoded, &alignment, &words, &weight,&vec_weight);
		GetLinearSymbolSequence(decoded, &alignment, &words, &weight);

		if (word_syms != NULL) {
			std::cerr << key << ' ';
			for (size_t i = 0; i < words.size(); i++) {
//					std::string ss = word_syms->Find(words[i]);
//					char buf[128];
//					sprintf(buf, "%d:%s ", words[i], ss.c_str());
//					std::string s = std::string(buf);
				std::string s = word_syms->Find(words[i]);
				if (s == "") {
					KALDI_LOG << "Word-id " << words[i] << ":"
							<< word_syms->Find(words[i - 1])
							<< " not in symbol table.";
				} else {
					vec_str_words.push_back(s);
					p_rec_result.push_back(s);
				}

				std::cerr << s << ' ';
			}
			std::cerr << endl;
		}

	} else {
		num_fail++;
		KALDI_WARN << "Did not successfully decode utterance " << key
				<< ", len = " << loglikes.NumRows() << endl;
	}

}
GinkgoNnet::GinkgoNnet(int argc, char *argv[]) {
	binary = true;
	acoustic_scale = 0.1;
	allow_partial = true;
	silent = false;
	Initialize(argc, argv);
}

GinkgoNnet::~GinkgoNnet() {
	if (word_syms)
		delete word_syms;
	delete decode_fst;
}

int GinkgoNnet::Initialize(int argc, char *argv[]) {
	try {
		using namespace kaldi;
		const char *usage =
				"Decode, reading log-likelihoods as matrices\n"
						" (model is needed only for the integer mappings in its transition-model)\n"
						"Usage:   decode-faster-mapped [options] model-in fst-in "
						"loglikes-rspecifier words-wspecifier [alignments-wspecifier]\n";
		ParseOptions po(usage);

		decoder_opts.Register(&po, true); // true == include obscure settings.
		nnet_opts.Register(&po);

		po.Register("binary", &binary, "Write output in binary mode");
		po.Register("acoustic-scale", &acoustic_scale,
				"Scaling factor for acoustic likelihoods");
		po.Register("allow-partial", &allow_partial,
				"Produce output even when final state was not reached");
		po.Register("word-symbol-table", &word_syms_filename,
				"Symbol table for words [for debug output]");
		po.Register("senone-symbol-table", &senone_syms_filename,
				"Symbol table for words [for debug output]");
		po.Register("silent", &silent, "Don't print any messages");

		po.Read(argc, argv);

		if (po.NumArgs() < 2 || po.NumArgs() > 5) {
			po.PrintUsage();
			exit(1);
		}

		model_in_filename = po.GetArg(1);
		fst_in_filename = po.GetArg(2);
		//feature_rspecifier = po.GetArg(3); //loglikes_rspecifier = po.GetArg(3),
		//words_wspecifier = po.GetArg(4);
		//alignment_wspecifier = po.GetOptArg(5);
		loglikes_rspecifier;

#if HAVE_CUDA==1
		if(use_gpu_id > -2)
		CuDevice::Instantiate().SelectGpuId(use_gpu_id);
#endif

		ReadKaldiObject(model_in_filename, &trans_model);
		if (word_syms_filename != "") {
			word_syms = fst::SymbolTable::ReadText(word_syms_filename);
			if (!word_syms)
				KALDI_ERR << "Could not read symbol table from file "
						<< word_syms_filename;
		}
		if (senone_syms_filename != "") {
			senone_syms = fst::SymbolTable::ReadText(senone_syms_filename);
			if (!senone_syms)
				KALDI_ERR << "Could not read senone table from file "
						<< senone_syms_filename;
		}

		// It's important that we initialize decode_fst after loglikes_reader, as it
		// can prevent crashes on systems installed without enough virtual memory.
		// It has to do with what happens on UNIX systems if you call fork() on a
		// large process: the page-table entries are duplicated, which requires a
		{
			Input ki(fst_in_filename.c_str());
			KALDI_LOG << "Loading decode fst: " << fst_in_filename;
			decode_fst = VectorFst<StdArc>::Read(ki.Stream(),
					fst::FstReadOptions(fst_in_filename));
			if (decode_fst == NULL) // fst code will warn.
				exit(1);
		}

		//// Initilize for nnet forward;
		if (!nnet_opts.loglikes_in) {
			GinkgoNnetHelper::InitNnet(nnet_opts, nnet_transf, nnet, priors);
		}

		BaseFloat tot_like = 0.0;
		kaldi::int64 frame_count = 0, num_success = 0, num_fail = 0;
		pdecoder = new FasterDecoder(*decode_fst, decoder_opts);
	} catch (const std::exception &e) {
		std::cerr << e.what();
		return -1;
	}
	return 0;
}
std::string GinkgoNnet::DecodeUtt(const char * data_rxfilename_) {
	return DecodeUtt(data_rxfilename_, acoustic_scale);
}
std::string GinkgoNnet::DecodeUtt(const char * data_rxfilename_,
		kaldi::BaseFloat acoustic_scale) {
	using namespace kaldi;
	try {
		Timer timer;

		KALDI_VLOG(2) << "Decode begin: ";

		FasterDecoder *pdecoder = new FasterDecoder(*decode_fst, decoder_opts);

		BaseFloat tot_like = 0.0;
		kaldi::int64 frame_count = 0, num_success = 0, num_fail = 0;
		bool allow_partial = true, silent = false;

		std::vector<std::string> rec;
		std::string rec_result = "";
		{
			KaldiObjectHolder<Matrix<BaseFloat> > feature_holder;
			Input data_input_;
			std::string key;
			{
				data_input_.Open(data_rxfilename_, NULL);
				std::istream &is = data_input_.Stream();
				is.clear(); // Clear any fail bits that may have been set... just in case
				is >> key; // This eats up any leading whitespace and gets the string.
				char c = is.peek();
				if (c != '\n')
					is.get(); // Consume the space or tab.
				feature_holder.Read(data_input_.Stream());
			}

			const Matrix<BaseFloat> &feature(feature_holder.Value());

			Timer timer2;
			Matrix<BaseFloat> loglikes;
			if (!nnet_opts.loglikes_in) {
				KALDI_VLOG(2) << "NnetForward... ";

#ifdef ANDROID
				//HJP:ADD:Time profiling
				Timer nnet_timer;
#endif
				GinkgoNnetHelper::NnetForward(nnet_opts, nnet_transf, nnet,
						priors, key, feature, loglikes);
#ifdef ANDROID
				KALDI_LOG << "GinkgoNnetHelper::NnetForward time: " << nnet_timer.Elapsed();
#endif
			} else {
				loglikes = feature;
			}
			//KALDI_LOG << "Decode... ";

#ifdef ANDROID
			//HJP:ADD:Time profiling
			Timer decode_timer;
#endif
			GinkgoNnetHelper::Decode(key, loglikes, *pdecoder, trans_model,
					word_syms, acoustic_scale, allow_partial, num_fail,
					num_success, frame_count, tot_like, silent, rec);
#ifdef ANDROID
			KALDI_LOG << "GinkgoNnetHelper::Decode time: " << decode_timer.Elapsed();
#endif

			kaldi::JoinVectorToString(rec, "", true, &rec_result);
			KALDI_VLOG(2) << "real-time factor assuming 100 frames/sec is "
					<< (timer2.Elapsed() * 100.0 / loglikes.NumRows());
		}
		delete pdecoder;
		double elapsed = timer.Elapsed();
		KALDI_VLOG(2) << "Time taken [excluding initialization] " << elapsed
				<< "s: real-time factor assuming 100 frames/sec is "
				<< (elapsed * 100.0 / frame_count);
		KALDI_VLOG(2) << "Done " << num_success << " utterances, failed for "
				<< num_fail;
		KALDI_VLOG(2) << "Overall log-likelihood per frame is "
				<< (tot_like / frame_count) << " over " << frame_count
				<< " frames.";

		return rec_result;
	} catch (const std::exception &e) {
		std::cerr << e.what();
		return e.what();
	}
}

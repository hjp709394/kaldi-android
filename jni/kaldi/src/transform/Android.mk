include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/transform
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-transform
LOCAL_SRC_FILES :=  balanced-cmvn.cc balanced-cmvn-test.cc basis-fmllr-diag-gmm.cc cmvn.cc compressed-transform-stats.cc exponential-transform.cc exponential-transform-test.cc fmllr-diag-gmm.cc fmllr-diag-gmm-test.cc fmpe.cc fmpe-test.cc hlda.cc lda-estimate.cc lda-estimate-test.cc lvtln.cc mllt.cc regression-tree.cc regression-tree-test.cc regtree-fmllr-diag-gmm.cc regtree-fmllr-diag-gmm-test.cc regtree-mllr-diag-gmm.cc regtree-mllr-diag-gmm-test.cc transform-common.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  kaldi-gmm  kaldi-tree  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

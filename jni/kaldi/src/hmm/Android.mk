include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/hmm
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-hmm
LOCAL_SRC_FILES :=  hmm-topology.cc hmm-topology-test.cc hmm-utils.cc hmm-utils-test.cc transition-model.cc tree-accu.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-tree  kaldi-matrix  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

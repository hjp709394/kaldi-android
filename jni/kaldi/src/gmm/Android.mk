include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/gmm
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-gmm
LOCAL_SRC_FILES :=  am-diag-gmm.cc am-diag-gmm-test.cc diag-gmm.cc diag-gmm-normal.cc diag-gmm-test.cc ebw-diag-gmm.cc ebw-diag-gmm-test.cc full-gmm.cc full-gmm-normal.cc full-gmm-test.cc indirect-diff-diag-gmm.cc mle-am-diag-gmm.cc mle-am-diag-gmm-test.cc mle-diag-gmm.cc mle-diag-gmm-test.cc mle-full-gmm.cc mle-full-gmm-test.cc model-common.cc model-test-common.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  kaldi-tree  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

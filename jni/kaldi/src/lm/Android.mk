include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/lm
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-lm
LOCAL_SRC_FILES :=  kaldi-lm.cc kaldi-lmtable.cc lm-lib-test.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  fst
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

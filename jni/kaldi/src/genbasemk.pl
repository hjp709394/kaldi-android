#! /bin/perl

$lib_prefix="kaldi-";
$r_path="kaldi/src";

#dependencies
%dep_hash = ("base" => "", "matrix" => "base", "util" => "base matrix", "thread" => "util", "feat" => "base matrix util gmm transform", "tree" => "base util matrix", "optimization" => "base matrix", "gmm" => "base util matrix tree", "tied" => "base util matrix gmm tree transform", "transform" => "base util matrix gmm tree", "sgmm" => "base util matrix gmm tree transform thread hmm", "sgmm2" => "base util matrix gmm tree transform thread hmm", "fstext" => "base util matrix tree", "hmm" => "base tree matrix", "lm" => "base util", "decoder" => "base util matrix gmm sgmm hmm tree transform", "lat" => "base util hmm", "cudamatrix" => "base util matrix", "nnet" => "base util matrix cudamatrix", "nnet-cpu" => "base util matrix thread");

$ext_include = "\$(TOP_PATH)/openfst/src/include/"; 
#%include_hash = ("fstext" => "\\\$(TOP_PATH)/openfst/src/include/", "lm" => "\\\$(TOP_PATH)/openfst/src/include/");
%ext_dep_hash = ("fstext" => "fst", "lm" => "fst");

# generating
$directories=`ls`;

while (<*>) {
	if ( -d $_ and exists $dep_hash{$_}) {
    $curdir = $_;
    @dfl = split( /\s+/, $dep_hash{$curdir} ); #dependencis file list
    $dep = "";
    for $f (@dfl) {
      $dep = "$dep $lib_prefix$f ";
    }


		$fl=`ls $curdir | grep -E "\.(cpp|cc|c|cxx)\$"`;  #file list
    @flarr = split( /\s+/, $fl ); #file list array
    print $flarr;
		$filelist="";
		for $t (@flarr) {
      $filelist = "$filelist $t";
    }

    print "$curdir \ndependencies: $dep\n";
    print "file list: $filelist\n";
    print "ext dep: $ext_dep_hash{$curdir}\n\n";

	open( fo, "> $curdir/Android.mk" ) or die "can not write $curdir/Android.mk";
	print fo "include \$(CLEAR_VARS)\n";
	print fo "LOCAL_PATH := \$(TOP_PATH)/$r_path/$curdir\n";
	print fo "LOCAL_C_INCLUDES += \$(TOP_PATH)/$r_path $ext_include\n";
	print fo "LOCAL_MODULE    := kaldi-$curdir\n";
	print fo "LOCAL_SRC_FILES := $filelist\n";
	print fo "LOCAL_STATIC_LIBRARIES := $dep $ext_dep_hash{$curdir}\n";
	print fo "LOCAL_LDLIBS := -L\$(SYSROOT)/usr/lib\n";
	print fo "include \$(BUILD_STATIC_LIBRARY)\n";
	close( fo );
	
		# system( "echo \"include \\\$(CLEAR_VARS)\" > $curdir/Android.mk" );
		# system( "echo \"LOCAL_PATH := \\\$(TOP_PATH)/$r_path/$curdir\" >> $curdir/Android.mk" );
		# system( "echo \"LOCAL_C_INCLUDES += \\\$(TOP_PATH)/$r_path $ext_include\" >> $curdir/Android.mk" );
		# system( "echo \"LOCAL_MODULE    := kaldi-$curdir\" >> $curdir/Android.mk" );
		# system( "echo \"LOCAL_SRC_FILES := $filelist\" >> $curdir/Android.mk" );
		# system( "echo \"LOCAL_STATIC_LIBRARIES := $dep $ext_dep_hash{$curdir}\" >> $curdir/Android.mk" );
		# system( "echo \"LOCAL_STATIC_LIBRARIES := \" >> $curdir/Android.mk" );	# only header file dependencis, which is seldom modified
		# system( "echo \"LOCAL_LDLIBS := -L\\\$(SYSROOT)/usr/lib\" >> $curdir/Android.mk" );
		# system( "echo \"include \\\$(BUILD_STATIC_LIBRARY)\" >> $curdir/Android.mk" );
	}
}


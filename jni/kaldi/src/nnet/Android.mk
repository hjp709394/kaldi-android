include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/nnet
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-nnet
LOCAL_SRC_FILES :=  nnet-cache.cc nnet-cache-tgtmat.cc nnet-component.cc nnet-loss.cc nnet-loss-prior.cc nnet-nnet.cc nnet-test.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  kaldi-cudamatrix  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

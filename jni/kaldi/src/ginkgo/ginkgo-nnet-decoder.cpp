// ginkgo/ginkgo-nnet-decoder.cc

// Copyright 2009-2011  Microsoft Corporation

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.


#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "tree/context-dep.h"
#include "hmm/transition-model.h"
#include "fstext/fstext-lib.h"
#include "decoder/faster-decoder.h"
#include "decoder/decodable-matrix.h"
#include "util/timer.h"
#include "nnet/nnet-nnet.h"
#include "nnet/nnet-loss.h"
#include "lat/kaldi-lattice.h" // for {Compact}LatticeArc



struct NnetForwardOptions {

	std::string feature_transform;
	std::string class_frame_counts;
	std::string nnet_model;
	bool apply_log;
	bool no_softmax;
	bool htk_in;
	bool loglikes_in;
	kaldi::BaseFloat prior_scale;

	NnetForwardOptions(): apply_log(false),
		no_softmax(false), htk_in(false), 
		loglikes_in(false), prior_scale(1.0){}

	void Register(kaldi::ParseOptions *po)
	{
		po->Register("feature-transform", &feature_transform, "Feature transform Neural Network");
		po->Register("class-frame-counts", &class_frame_counts, "Counts of frames for posterior division by class-priors");
		po->Register("apply-log", &apply_log, "Transform MLP output to logscale");
		po->Register("no-softmax", &no_softmax, "No softmax on MLP output. The MLP outputs directly log-likelihoods, log-priors will be subtracted");
		po->Register("nnet-model", &nnet_model, "nnet models");
		po->Register("htk-in", &htk_in, "Read input as HTK features");
		po->Register("loglikes-in", &loglikes_in, "Read input as loglikes");
		po->Register("prior-scale", &prior_scale, "scaling factor of prior log-probabilites given by --class-frame-counts");
	}
};

void InitNnet(NnetForwardOptions &nnet_opts, 
	kaldi::Nnet &nnet_transf, 
	kaldi::Nnet &nnet, 
	kaldi::CuVector<kaldi::BaseFloat> &priors)
{
	typedef kaldi::int32 int32;
	typedef kaldi::BaseFloat BaseFloat;
	
	//Nnet nnet_transf;
	if(nnet_opts.feature_transform != "") {
		KALDI_LOG << "Loading feature_transform: " << nnet_opts.feature_transform;
		nnet_transf.Read(nnet_opts.feature_transform);
	}

	//Nnet nnet;
	KALDI_LOG << "Loading Nnet: " << nnet_opts.nnet_model;
	nnet.Read(nnet_opts.nnet_model);

	kaldi::Vector<kaldi::BaseFloat> tmp_priors;
	if(nnet_opts.class_frame_counts != "") {
		kaldi::Input in;
		in.OpenTextMode(nnet_opts.class_frame_counts);
		tmp_priors.Read(in.Stream(), false);
		in.Close();

		//create inv. priors, or log inv priors 
		BaseFloat sum = tmp_priors.Sum();
		tmp_priors.Scale(1.0/sum);
		if (nnet_opts.apply_log || nnet_opts.no_softmax) {
			tmp_priors.ApplyLog();
			tmp_priors.Scale(-nnet_opts.prior_scale);
		} else {
			tmp_priors.ApplyPow(-nnet_opts.prior_scale);
		}

		//detect the inf, replace by something reasonable (maximal non-inf value)
		//a) replace inf by -inf
		for(int32 i=0; i<tmp_priors.Dim(); i++) {
			if(tmp_priors(i) == std::numeric_limits<BaseFloat>::infinity()) {
				tmp_priors(i) *= -1.0;
			}
		}
		//b) find max
		BaseFloat max = tmp_priors.Max();
		//c) replace -inf by max prior
		for(int32 i=0; i<tmp_priors.Dim(); i++) {
			if(tmp_priors(i) == -std::numeric_limits<BaseFloat>::infinity()) {
				tmp_priors(i) = max;
			}
		}

		// push priors to GPU
		priors.Resize(tmp_priors.Dim());
		priors.CopyFromVec(tmp_priors);
	}
}

void NnetForward(NnetForwardOptions &nnet_opts, 
	kaldi::Nnet &nnet_transf, 
	kaldi::Nnet &nnet, 
	kaldi::CuVector<kaldi::BaseFloat> &priors,
	std::string key,
	const kaldi::MatrixBase<kaldi::BaseFloat> &mat,
	kaldi::Matrix<kaldi::BaseFloat> &nnet_out_host)
{
	typedef kaldi::int32 int32;
	typedef kaldi::BaseFloat BaseFloat;

	// check for NaN/inf
	for(int32 r=0; r<mat.NumRows(); r++) {
		for(int32 c=0; c<mat.NumCols(); c++) {
			BaseFloat val = mat(r,c);
			if(val != val) KALDI_ERR << "NaN in features of : " << key;
			if(val == std::numeric_limits<BaseFloat>::infinity())
				KALDI_ERR << "inf in features of : " << key;
		}
	}
	// push it to gpu
	kaldi::CuMatrix<BaseFloat> feats, feats_transf, nnet_out;
	
	feats = mat;
	// fwd-pass
	nnet_transf.Feedforward(feats, &feats_transf);
	nnet.Feedforward(feats_transf, &nnet_out);

	// convert posteriors to log-posteriors
	if (nnet_opts.apply_log) {
		nnet_out.ApplyLog();
	}

	// divide posteriors by priors to get quasi-likelihoods
	if(nnet_opts.class_frame_counts != "") {
		if (nnet_opts.apply_log || nnet_opts.no_softmax) {
			nnet_out.AddVecToRows(1.0, priors, 1.0);
		} else {
			nnet_out.MulColsVec(priors);
		}
	}

	//download from GPU
	nnet_out_host.Resize(nnet_out.NumRows(), nnet_out.NumCols());
	nnet_out.CopyToMat(&nnet_out_host);
	
	//check for NaN/inf
	for(int32 r=0; r<nnet_out_host.NumRows(); r++) {
		for(int32 c=0; c<nnet_out_host.NumCols(); c++) {
			BaseFloat val = nnet_out_host(r,c);
			if(val != val) KALDI_ERR << "NaN in NNet output of : " << key;
			if(val == std::numeric_limits<BaseFloat>::infinity())
				KALDI_ERR << "inf in NNet cout put of : " << key;
		}
	}
}


bool Decode(std::string key, 
	kaldi::Matrix<kaldi::BaseFloat> &loglikes,
	kaldi::FasterDecoder &decoder,
	kaldi::TransitionModel &trans_model,
	kaldi::Int32VectorWriter &words_writer,
	kaldi::Int32VectorWriter &alignment_writer,
	fst::SymbolTable *word_syms,
	kaldi::BaseFloat acoustic_scale,
	bool allow_partial,
	kaldi::int64 &num_fail,
	kaldi::int64 &num_success,
	kaldi::int64 &frame_count,
	kaldi::BaseFloat &tot_like,
	bool silent)
{
	using namespace kaldi;
	using fst::SymbolTable;
	using fst::VectorFst;
	using fst::StdArc;

	if (loglikes.NumRows() == 0) {
		KALDI_WARN << "Zero-length utterance: " << key;
		num_fail++;
		return true;
	}

	DecodableMatrixScaledMapped decodable(trans_model, loglikes, acoustic_scale);
	decoder.Decode(&decodable);

	//decoder
	VectorFst<LatticeArc> decoded;  // linear FST.

	if ( (allow_partial || decoder.ReachedFinal())
		&& decoder.GetBestPath(&decoded) ) 
	{
			num_success++;
			if (!decoder.ReachedFinal())
				KALDI_WARN << "Decoder did not reach end-state, outputting partial traceback.";

			std::vector<kaldi::int32> alignment;
			std::vector<kaldi::int32> words;
			LatticeWeight weight;
			frame_count += loglikes.NumRows();

			GetLinearSymbolSequence(decoded, &alignment, &words, &weight);

			words_writer.Write(key, words);
			words_writer.Flush();
			for (size_t i = 0; i < alignment.size(); i++)
				alignment[i] = trans_model.TransitionIdToPdf(alignment[i]);
			if (alignment_writer.IsOpen())
				alignment_writer.Write(key, alignment);
			if (word_syms != NULL) {
				std::cerr << key << ' ';
				for (size_t i = 0; i < words.size(); i++) {
					std::string s = word_syms->Find(words[i]);
					if (s == "")
						KALDI_ERR << "Word-id " << words[i] <<" not in symbol table.";
					std::cerr << s << ' ';
				}
				std::cerr << '\n';
			}
			BaseFloat like = -weight.Value1() -weight.Value2();
			tot_like += like;
			if (!silent) KALDI_LOG << "Log-like per frame for utterance " << key << " is "
				<< (like / loglikes.NumRows()) << " over "
				<< loglikes.NumRows() << " frames.";

	} else {
		num_fail++;
		KALDI_WARN << "Did not successfully decode utterance " << key
			<< ", len = " << loglikes.NumRows();
	}

}

int main(int argc, char *argv[]) {
	try {
		using namespace kaldi;
		typedef kaldi::int32 int32;
		using fst::SymbolTable;
		using fst::VectorFst;
		using fst::StdArc;

		const char *usage =
			"Decode, reading log-likelihoods as matrices\n"
			" (model is needed only for the integer mappings in its transition-model)\n"
			"Usage:   decode-faster-mapped [options] model-in fst-in "
			"loglikes-rspecifier words-wspecifier [alignments-wspecifier]\n";
		ParseOptions po(usage);
		bool binary = true;
		BaseFloat acoustic_scale = 0.1;
		bool allow_partial = true, silent = false;

		std::string word_syms_filename;

		FasterDecoderOptions decoder_opts;		
		decoder_opts.Register(&po, true);  // true == include obscure settings.
		NnetForwardOptions nnet_opts;
		nnet_opts.Register(&po);

		po.Register("binary", &binary, "Write output in binary mode");
		po.Register("acoustic-scale", &acoustic_scale, "Scaling factor for acoustic likelihoods");
		po.Register("allow-partial", &allow_partial, "Produce output even when final state was not reached");
		po.Register("word-symbol-table", &word_syms_filename, "Symbol table for words [for debug output]");
		po.Register("silent", &silent, "Don't print any messages");

		po.Read(argc, argv);

		if (po.NumArgs() < 4 || po.NumArgs() > 5) {
			po.PrintUsage();
			exit(1);
		}

		std::string model_in_filename = po.GetArg(1),
			fst_in_filename = po.GetArg(2),
			feature_rspecifier = po.GetArg(3), //loglikes_rspecifier = po.GetArg(3),
			words_wspecifier = po.GetArg(4),
			alignment_wspecifier = po.GetOptArg(5),
			loglikes_rspecifier;

#if HAVE_CUDA==1
		if(use_gpu_id > -2)
			CuDevice::Instantiate().SelectGpuId(use_gpu_id);
#endif

		TransitionModel trans_model;
		ReadKaldiObject(model_in_filename, &trans_model);

		Int32VectorWriter words_writer(words_wspecifier);

		Int32VectorWriter alignment_writer(alignment_wspecifier);

		fst::SymbolTable *word_syms = NULL;
		if (word_syms_filename != "") {
			word_syms = fst::SymbolTable::ReadText(word_syms_filename);
			if (!word_syms)
				KALDI_ERR << "Could not read symbol table from file "<<word_syms_filename;
		}

		// It's important that we initialize decode_fst after loglikes_reader, as it
		// can prevent crashes on systems installed without enough virtual memory.
		// It has to do with what happens on UNIX systems if you call fork() on a
		// large process: the page-table entries are duplicated, which requires a
		// lot of virtual memory.
		VectorFst<StdArc> *decode_fst = NULL; 
		{
			Input ki(fst_in_filename.c_str());
			KALDI_LOG << "Loading decode fst: " << fst_in_filename;
			decode_fst =
				VectorFst<StdArc>::Read(ki.Stream(), fst::FstReadOptions(fst_in_filename));
			if (decode_fst == NULL) // fst code will warn.
				exit(1);
		}

		//// Initilize for nnet forward;
		Nnet nnet_transf, nnet;
		CuVector<BaseFloat> priors;
		if (!nnet_opts.loglikes_in) {
			InitNnet(nnet_opts, nnet_transf, nnet, priors);
		}

		BaseFloat tot_like = 0.0;
		kaldi::int64 frame_count = 0, num_success = 0, num_fail = 0;
		FasterDecoder decoder(*decode_fst, decoder_opts);

		Timer timer;

		KALDI_LOG << "Decode begin: ";
		if (nnet_opts.htk_in)
		{
			SequentialTableReader<HtkMatrixHolder> htk_reader(feature_rspecifier);
			for (; !htk_reader.Done(); htk_reader.Next()){
				Timer timer2;
				std::string key = htk_reader.Key();
				const Matrix<BaseFloat> &feature (htk_reader.Value().first);
				//SubMatrix<BaseFloat> subfeature=feature.RowRange(1,10);
				Matrix<BaseFloat> loglikes;
				//KALDI_LOG << "NnetForward... ";
				if (!nnet_opts.loglikes_in) {
					NnetForward(nnet_opts, nnet_transf, nnet, priors, key, feature, loglikes);
				} else {
					loglikes = feature;
				}

				//KALDI_LOG << "Decode... ";
				Decode(key, loglikes, decoder, trans_model, words_writer, 
					alignment_writer, word_syms, acoustic_scale, allow_partial,
					num_fail, num_success, frame_count, tot_like, silent);
				
				if(!silent) KALDI_LOG << "real-time factor assuming 100 frames/sec is "
					<< (timer2.Elapsed()*100.0/loglikes.NumRows());
			}
		}
		else
		{
			SequentialBaseFloatMatrixReader feature_reader(feature_rspecifier);
			for (; !feature_reader.Done(); feature_reader.Next()) {
				Timer timer2;

				std::string key = feature_reader.Key();
				const Matrix<BaseFloat> &feature (feature_reader.Value());
				Matrix<BaseFloat> loglikes;
				if (!nnet_opts.loglikes_in) {
					NnetForward(nnet_opts, nnet_transf, nnet, priors, key, feature, loglikes);
				} else {
					loglikes = feature;
				}

				Decode(key, loglikes, decoder, trans_model, words_writer, 
					alignment_writer, word_syms, acoustic_scale, allow_partial,
					num_fail, num_success, frame_count, tot_like, silent);

				if(!silent) KALDI_LOG << "real-time factor assuming 100 frames/sec is "
					<< (timer2.Elapsed()*100.0/loglikes.NumRows());
			}

		}

		double elapsed = timer.Elapsed();
		if(!silent) KALDI_LOG << "Time taken [excluding initialization] "<< elapsed
			<< "s: real-time factor assuming 100 frames/sec is "
			<< (elapsed*100.0/frame_count);
		if(!silent) KALDI_LOG << "Done " << num_success << " utterances, failed for "
			<< num_fail;
		if(!silent) KALDI_LOG << "Overall log-likelihood per frame is " << (tot_like/frame_count)
			<< " over " << frame_count << " frames.";

		if (word_syms) delete word_syms;
		delete decode_fst;
		if (num_success != 0) return 0;
		else return 1;
	} catch(const std::exception &e) {
		std::cerr << e.what();
		return -1;
	}
}



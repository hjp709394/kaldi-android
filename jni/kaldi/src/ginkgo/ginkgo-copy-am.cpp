


#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "gmm/am-diag-gmm.h"
#include "hmm/hmm-topology.h"
#include "hmm/transition-model.h"
#include "tree/build-tree.h"
#include "fstext/fstext-lib.h"
#include "gmm/diag-gmm-normal.h"


namespace Ginkgo{
int WriteHtk(fst::SymbolTable *phone_syms, std::ostream &out_stream)
{
	typedef kaldi::int32 int32;
	for (int32 i=0;i<phone_syms->NumSymbols();i++) {
		std::string s = phone_syms->Find(i);
		if (s == "")
			continue;
		std::string trans_name = "T_"+s;
		out_stream << "~t \"" << trans_name << "\"\n";
		if (s == "sil"){
			out_stream << "<TRANSP> 5\n";
			out_stream << "0.0 1.0 0.0 0.0 0.0\n";
			out_stream << "0.0 0.4 0.4 0.2 0.0\n";
			out_stream << "0.0 0.0 0.5 0.5 0.0\n";
			out_stream << "0.0 0.2 0.0 0.4 0.4\n";
			out_stream << "0.0 0.0 0.0 0.5 0.0\n";
			continue;
		}
		if (s == "sp"){
			out_stream << "<TRANSP> 3\n";
			out_stream << "0.0 0.7 0.3\n";
			out_stream << "0.0 0.7 0.3\n";
			out_stream << "0.0 0.0 0.0\n";
			continue;
		}
		{
			out_stream << "<TRANSP> 5\n";
			out_stream << "0.0 1.0 0.0 0.0 0.0\n";
			out_stream << "0.0 0.5 0.5 0.0 0.0\n";
			out_stream << "0.0 0.0 0.5 0.5 0.0\n";
			out_stream << "0.0 0.0 0.0 0.5 0.5\n";
			out_stream << "0.0 0.0 0.0 0.5 0.0\n";
		}
	}
	return 1;
}

void FixHTKDiagGConst(kaldi::DiagGmmNormal *dgmm, kaldi::Vector<kaldi::BaseFloat> *gconst) 
{
#define MINLARG 2.45E-308
#define LZERO -1.0E10
	using namespace kaldi;
	typedef kaldi::int32 int32;

	if (gconst->Dim() != dgmm->NumGauss())
		gconst->Resize(dgmm->NumGauss());

	//int32 num_mix = dgmm->NumGauss();
	int32 dim = dgmm->Dim();
	BaseFloat offset = M_LOG_2PI * dim;

	for (int i=0;i<dgmm->NumGauss();i++) {
		(*gconst)(i)=offset;
		for (int j=0;j<dim;j++){
			(*gconst)(i) += dgmm->vars_(i,j)<=MINLARG?LZERO:log(dgmm->vars_(i,j));
		}
	}

	return;
}

int WriteHtk(kaldi::DiagGmmNormal *dgmm, std::ostream &out_stream)
{
	using namespace kaldi;
	typedef kaldi::int32 int32;
	int32 nmix = dgmm->NumGauss(), dim = dgmm->Dim();

	if (nmix == 1) {
		WriteToken(out_stream, false, "<MEAN>");
		out_stream << dim << "\n";
		for (int32 j=0;j<dim;j++) out_stream << dgmm->means_(0,j) << " ";
		out_stream << "\n";
		WriteToken(out_stream, false, "<VARIANCE>");
		out_stream << dim << "\n";
		for (int32 j=0;j<dim;j++) out_stream << dgmm->vars_(0,j) << " ";
	} else {
		kaldi::Vector<BaseFloat> gconst (nmix);
		FixHTKDiagGConst(dgmm, &gconst);
		WriteToken(out_stream, false, "<NUMMIXES>");
		out_stream << nmix << "\n";
		for (int32 mix=0;mix< nmix;mix++) {
			WriteToken(out_stream, false, "<MIXTURE>");
			out_stream << mix << " " << dgmm->weights_(mix) << "\n";
			WriteToken(out_stream, false, "<MEAN>");
			out_stream << dim << "\n";
			for (int32 j=0;j<dim;j++) out_stream << dgmm->means_(mix,j) << " ";
			out_stream << "\n";
			WriteToken(out_stream, false, "<VARIANCE>");
			out_stream << dim << "\n";
			for (int32 j=0;j<dim;j++) out_stream << dgmm->vars_(mix,j) << " ";
			out_stream << "\n";
			WriteToken(out_stream, false, "<GCONST>");
			out_stream << gconst(mix) << "\n";
		}
	}
	return 1;
}

std::string ToString(int i){
	char s[1024];	
	sprintf(s, "%d", i);
	return string(s);
}

int WriteHtk(kaldi::AmDiagGmm *am, std::ostream &out_stream)
{
	typedef kaldi::int32 int32;
	for (int32 i=0;i<am->NumPdfs();i++) {
		kaldi::DiagGmmNormal dgmm(am->GetPdf(i));
		std::string states_name = "S"+ToString(i);
		kaldi::WriteToken(out_stream, false, "~s");
		out_stream << " \"" << states_name << "\"\n";
		WriteHtk(&dgmm, out_stream);
	}
	
	return 1;
}

std::vector<std::string> xtokenize(char *buf, const char *sep = "\n\r\t-+ ") {
	std::vector<std::string> ret;
	char *p;
	char *start;
	for(start=p=buf; *p; p++) {
		int k;
		for(k=0; sep[k]; k++)
			if( p[0] == sep[k] ) {
				// found one
				*p = 0;
				if( start != p ) ret.push_back(start); // add if previous char was not a sep
				start = p+1;
				break;
			}
	}
	if( p != start )
		ret.push_back(start);
	return ret;
}

std::vector<std::string> xtokenize(const std::string str, const char *sep = "\n\r\t-+ ") 
{
	return xtokenize((char*)str.c_str(), sep);
}

void ReadCdphoneList(std::istream &is, fst::SymbolTable *phone_syms, std::map<std::string, std::vector<int > > &cdphones, kaldi::int32 &nphones)
{
	using namespace kaldi;
	typedef kaldi::int32 int32;
	std::string line;
	nphones=0;
	while (getline(is, line)) {
		std::vector<std::string> ctx;
		ctx=xtokenize(line);
		
		int32 ctx_size=ctx.size();
		nphones++;
		std::vector<int> ctx_int(ctx_size);
		std::string phone_name = "";
		if (ctx_size == 3) {
			phone_name = ctx[0]+"-"+ctx[1]+"+"+ctx[2];
		}

		for (int32 i=0;i<static_cast<int32>(ctx.size());i++) {
			int32 pid = static_cast<int32>(phone_syms->Find(ctx[i]));
			if (pid == -1)
				KALDI_ERR << "unknown phone " << ctx[i];
			ctx_int[i]=pid;
		}
		cdphones.insert(make_pair(phone_name, ctx_int));
	}
}

void OutputCdphoneHmm(const kaldi::HmmTopology &topo, kaldi::ContextDependency &ctx_dep,
	std::map<std::string, std::vector<kaldi::int32 > > &cdphones, fst::SymbolTable *phone_syms,
	std::ostream &out_stream, kaldi::int32 central_postion=1)
{
	using namespace kaldi;
	typedef kaldi::int32 int32;

	std::map<std::string, std::vector<kaldi::int32 > >::iterator pit = cdphones.begin(), 
		pe = cdphones.end();
	for (;pit!=pe;pit++) {
		std::string phone_name = pit->first;

		std::vector<int32> &ctx_window = pit->second;
		int32 central_phoneid = ctx_window[central_postion];
		int32 num_pdfs = topo.NumPdfClasses(central_phoneid);
		
		out_stream << "~h \"" << phone_name << "\"\n<BEGINHMM>\n<NUMSTATES> "<<num_pdfs+2<<"\n";
		for (int32 pdfclass=0;pdfclass<num_pdfs;pdfclass++) {
			int32 pdf_id;
			if (!ctx_dep.Compute(ctx_window, pdfclass, &pdf_id))
				KALDI_ERR<< "Can not find the pdf id";
			std::string states_name = "S"+ToString(pdf_id);
			out_stream << "<STATE> " << pdfclass+2 << "\n~s \"" << states_name << "\"\n";
		}
		std::string s = phone_syms->Find(central_phoneid);
		if (s == "")
			KALDI_ERR<< "Can not find the central_phoneid id";;
		out_stream << "~t \"" << "T_"+s << "\"\n<ENDHMM>\n";
	}
	
}

};

int main(int argc, char *argv[]) {
	try {
		using namespace Ginkgo;
		using namespace kaldi;
		using kaldi::int32;

		const char *usage =
			"Convert Kaldi am to HTK format with Kaldi Tree\n"
			"Usage:  ginkgo-copy-am <mdl-in> <tree-in> <phone-syms-in> <mmf-out>\n"
			" e.g. ginkgo-copy-am --cdphone-list=triphones.phf 1.mdl tree phones.txt mmf";

		std::string cdphone_list_filename;
		kaldi::int32 central_postion=1;
		ParseOptions po(usage);
		po.Register("cdphone-list", &cdphone_list_filename, "Symbol table for phone");
		po.Register("central-postion", &central_postion, "central postion for cdphone, default=1");

		po.Read(argc, argv);

		if (po.NumArgs() != 4) {
			po.PrintUsage();
			exit(1);
		}

		std::string model_rxfilename = po.GetArg(1),
			tree_filename = po.GetArg(2),
			phone_syms_filename = po.GetArg(3),
			model_out_filename = po.GetArg(4);

		TransitionModel trans_model;
		AmDiagGmm am_gmm;
		{
			bool binary;
			Input ki(model_rxfilename, &binary);
			trans_model.Read(ki.Stream(), binary);
			am_gmm.Read(ki.Stream(), binary);
		}


		ContextDependency ctx_dep;
		ReadKaldiObject(tree_filename, &ctx_dep);

		fst::SymbolTable *phone_syms = NULL;
		if (!(phone_syms = fst::SymbolTable::ReadText(phone_syms_filename)))
			KALDI_ERR << "Could not read symbol table from file " << phone_syms_filename;

		std::map<std::string, std::vector<int > > cdphones;
		kaldi::int32 nphones;
		if (cdphone_list_filename != "") {
			Input ki(cdphone_list_filename, false);
			ReadCdphoneList(ki.Stream(), phone_syms, cdphones, nphones);
		}
		
		Output ko(model_out_filename, false);
		{
			WriteHtk(phone_syms, ko.Stream());
			WriteHtk(&am_gmm, ko.Stream());
			if (cdphone_list_filename != "") {
				const kaldi::HmmTopology &topo = trans_model.GetTopo();
				OutputCdphoneHmm(topo, ctx_dep, cdphones, phone_syms,ko.Stream(), central_postion);
			}
		}

		return 0;
	} catch(const std::exception &e) {
		std::cerr << e.what();
		return -1;
	}
}



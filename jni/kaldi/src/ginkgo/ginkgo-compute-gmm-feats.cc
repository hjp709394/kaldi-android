// featbin/compute-fbank-feats.cc

// Copyright 2009-2012  Microsoft Corporation
//                      Johns Hopkins University (author: Daniel Povey)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "feat/feature-fbank.h"
#include "feat/wave-reader.h"
#include "base/kaldi-common.h"
#include "gmm/am-diag-gmm.h"
#include "hmm/transition-model.h"


int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
	typedef kaldi::int32 int32;
    const char *usage =
        "Create Gaussian Feature.\n"
        "Usage:  ginkgo-compute-gmm-feats [options...] model_rxfilename feats-rspecifier feats-rspecifier\n";

    ParseOptions po(usage);
	bool loglik = true;

	po.Register("loglik", &loglik, "Output loglik");
    po.Read(argc, argv);

    if (po.NumArgs() != 3) {
      po.PrintUsage();
      exit(1);
    }
	
	
	std::string model_rxfilename = po.GetArg(1),
		feature_rspecifier = po.GetArg(2),
		output_wspecifier = po.GetArg(3);

	TransitionModel trans_model;
	AmDiagGmm am_gmm;
	{
		bool binary;
		Input ki(model_rxfilename, &binary);
		trans_model.Read(ki.Stream(), binary);
		am_gmm.Read(ki.Stream(), binary);
	}

	int32 num_fail = 0, num_success = 0;
	int32 num_pdf = am_gmm.NumPdfs();

	SequentialBaseFloatMatrixReader feature_reader(feature_rspecifier);
	BaseFloatMatrixWriter kaldi_writer;  // typedef to TableWriter<something>.

	if (!kaldi_writer.Open(output_wspecifier))
		KALDI_ERR << "Could not initialize output with wspecifier "
		<< output_wspecifier;

	for (; !feature_reader.Done(); feature_reader.Next()) {
		std::string utt = feature_reader.Key();
		Matrix<BaseFloat> in_features (feature_reader.Value());
		feature_reader.FreeCurrent();
		if (in_features.NumRows() == 0) {
			KALDI_WARN << "Zero-length utterance: " << utt;
			num_fail++;
			continue;
		}

		Matrix<BaseFloat> out_features;
		out_features.Resize(in_features.NumRows(), num_pdf);

		for (int32 r=0;r<in_features.NumRows();r++) {
			for (int32 c=0;c<num_pdf;c++) {
				out_features(r,c)=am_gmm.LogLikelihood(c, in_features.Row(r));
				if (!loglik) 
					out_features(r,c)=exp(out_features(r,c));
			}
		}
		kaldi_writer.Write(utt, out_features);

		num_success++;
	}

	KALDI_LOG << " Done " << num_success << " out of " << " utterances, failed for " << num_fail;
    return (num_success != 0 ? 0 : 1);
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
  return 0;
}


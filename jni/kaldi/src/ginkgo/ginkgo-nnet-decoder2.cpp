// bin/decode-faster-mapped.cc

// Copyright 2009-2011  Microsoft Corporation

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.


#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "tree/context-dep.h"
#include "hmm/transition-model.h"
#include "fstext/fstext-lib.h"
#include "decoder/decodable-matrix.h"
#include "decoder/lattice-faster-decoder.h"
#include "util/timer.h"
#include "nnet/nnet-nnet.h"
#include "nnet/nnet-loss.h"
#include "lat/kaldi-lattice.h" // for {Compact}LatticeArc
#include "lat/lattice-functions.h"



struct NnetForwardOptions {

	std::string feature_transform;
	std::string class_frame_counts;
	std::string nnet_model;
	bool apply_log;
	bool no_softmax;
	bool htk_in;
	bool loglikes_in;
	kaldi::BaseFloat prior_scale;

	NnetForwardOptions(): apply_log(false),
		no_softmax(false), htk_in(false), 
		loglikes_in(false), prior_scale(1.0){}

	void Register(kaldi::ParseOptions *po)
	{
		po->Register("feature-transform", &feature_transform, "Feature transform Neural Network");
		po->Register("class-frame-counts", &class_frame_counts, "Counts of frames for posterior division by class-priors");
		po->Register("apply-log", &apply_log, "Transform MLP output to logscale");
		po->Register("no-softmax", &no_softmax, "No softmax on MLP output. The MLP outputs directly log-likelihoods, log-priors will be subtracted");
		po->Register("nnet-model", &nnet_model, "nnet models");
		po->Register("htk-in", &htk_in, "Read input as HTK features");
		po->Register("loglikes-in", &loglikes_in, "Read input as loglikes");
		po->Register("prior-scale", &prior_scale, "scaling factor of prior log-probabilites given by --class-frame-counts");
	}
};

void InitNnet(NnetForwardOptions &nnet_opts, 
	kaldi::Nnet &nnet_transf, 
	kaldi::Nnet &nnet, 
	kaldi::CuVector<kaldi::BaseFloat> &priors)
{
	typedef kaldi::int32 int32;
	typedef kaldi::BaseFloat BaseFloat;
	//Nnet nnet_transf;
	if(nnet_opts.feature_transform != "") {
		KALDI_LOG << "Loading feature_transform: " << nnet_opts.feature_transform;
		nnet_transf.Read(nnet_opts.feature_transform);
	}

	//Nnet nnet;
	KALDI_LOG << "Loading Nnet: " << nnet_opts.nnet_model;
	nnet.Read(nnet_opts.nnet_model);

	kaldi::Vector<kaldi::BaseFloat> tmp_priors;
	if(nnet_opts.class_frame_counts != "") {
		kaldi::Input in;
		in.OpenTextMode(nnet_opts.class_frame_counts);
		tmp_priors.Read(in.Stream(), false);
		in.Close();

		//create inv. priors, or log inv priors 
		BaseFloat sum = tmp_priors.Sum();
		tmp_priors.Scale(1.0/sum);
		if (nnet_opts.apply_log || nnet_opts.no_softmax) {
			tmp_priors.ApplyLog();
			tmp_priors.Scale(-nnet_opts.prior_scale);
		} else {
			tmp_priors.ApplyPow(-nnet_opts.prior_scale);
		}

		//detect the inf, replace by something reasonable (maximal non-inf value)
		//a) replace inf by -inf
		for(int32 i=0; i<tmp_priors.Dim(); i++) {
			if(tmp_priors(i) == std::numeric_limits<BaseFloat>::infinity()) {
				tmp_priors(i) *= -1.0;
			}
		}
		//b) find max
		BaseFloat max = tmp_priors.Max();
		//c) replace -inf by max prior
		for(int32 i=0; i<tmp_priors.Dim(); i++) {
			if(tmp_priors(i) == -std::numeric_limits<BaseFloat>::infinity()) {
				tmp_priors(i) = max;
			}
		}

		// push priors to GPU
		priors.Resize(tmp_priors.Dim());
		priors.CopyFromVec(tmp_priors);
	}
}

void NnetForward(NnetForwardOptions &nnet_opts, 
	kaldi::Nnet &nnet_transf, 
	kaldi::Nnet &nnet, 
	kaldi::CuVector<kaldi::BaseFloat> &priors,
	std::string key,
	const kaldi::MatrixBase<kaldi::BaseFloat> &mat,
	kaldi::Matrix<kaldi::BaseFloat> &nnet_out_host)
{
	typedef kaldi::int32 int32;
	typedef kaldi::BaseFloat BaseFloat;

	// check for NaN/inf
	for(int32 r=0; r<mat.NumRows(); r++) {
		for(int32 c=0; c<mat.NumCols(); c++) {
			BaseFloat val = mat(r,c);
			if(val != val) KALDI_ERR << "NaN in features of : " << key;
			if(val == std::numeric_limits<BaseFloat>::infinity())
				KALDI_ERR << "inf in features of : " << key;
		}
	}
	// push it to gpu
	kaldi::CuMatrix<BaseFloat> feats, feats_transf, nnet_out;
	
	feats = mat;
	// fwd-pass
	nnet_transf.Feedforward(feats, &feats_transf);
	nnet.Feedforward(feats_transf, &nnet_out);

	// convert posteriors to log-posteriors
	if (nnet_opts.apply_log) {
		nnet_out.ApplyLog();
	}

	// divide posteriors by priors to get quasi-likelihoods
	if(nnet_opts.class_frame_counts != "") {
		if (nnet_opts.apply_log || nnet_opts.no_softmax) {
			nnet_out.AddVecToRows(1.0, priors, 1.0);
		} else {
			nnet_out.MulColsVec(priors);
		}
	}

	//download from GPU
	nnet_out_host.Resize(nnet_out.NumRows(), nnet_out.NumCols());
	nnet_out.CopyToMat(&nnet_out_host);
	
	//check for NaN/inf
	for(int32 r=0; r<nnet_out_host.NumRows(); r++) {
		for(int32 c=0; c<nnet_out_host.NumCols(); c++) {
			BaseFloat val = nnet_out_host(r,c);
			if(val != val) KALDI_ERR << "NaN in NNet output of : " << key;
			if(val == std::numeric_limits<BaseFloat>::infinity())
				KALDI_ERR << "inf in NNet cout put of : " << key;
		}
	}
}

bool GinkgoDecodeUtterance(
	kaldi::LatticeFasterDecoder &decoder, // not const but is really an input.
	kaldi::DecodableInterface &decodable, // not const but is really an input.
	const fst::SymbolTable *word_syms,
	std::string utt,
	double acoustic_scale,
	bool determinize,
	bool allow_partial,
	kaldi::Int32VectorWriter *alignment_writer,
	kaldi::Int32VectorWriter *words_writer,
	kaldi::PosteriorWriter *posterior_writer,
	double *like_ptr) 
{ // puts utterance's like in like_ptr on success.
	using namespace kaldi;
	typedef kaldi::int32 int32;
	using fst::VectorFst;

	if (!decoder.Decode(&decodable)) {
		KALDI_WARN << "Failed to decode file " << utt;
		return false;
	}
	if (!decoder.ReachedFinal()) {
		if (allow_partial) {
			KALDI_WARN << "Outputting partial output for utterance " << utt
				<< " since no final-state reached\n";
		} else {
			KALDI_WARN << "Not producing output for utterance " << utt
				<< " since no final-state reached and "
				<< "--allow-partial=false.\n";
			return false;
		}
	}

	double likelihood;
	LatticeWeight weight;
	int32 num_frames;
	{ // First do some stuff with word-level traceback...
		VectorFst<LatticeArc> decoded;
		if (!decoder.GetBestPath(&decoded)) 
			// Shouldn't really reach this point as already checked success.
			KALDI_ERR << "Failed to get traceback for utterance " << utt;

		std::vector<int32> alignment;
		std::vector<int32> words;
		GetLinearSymbolSequence(decoded, &alignment, &words, &weight);
		num_frames = alignment.size();
		if (words_writer->IsOpen())
			words_writer->Write(utt, words);
		if (alignment_writer->IsOpen())
			alignment_writer->Write(utt, alignment);
		if (word_syms != NULL) {
			std::cerr << utt << ' ';
			for (size_t i = 0; i < words.size(); i++) {
				std::string s = word_syms->Find(words[i]);
				if (s == "")
					KALDI_ERR << "Word-id " << words[i] <<" not in symbol table.";
				std::cerr << s << ' ';
			}
			std::cerr << '\n';
		}
		likelihood = -(weight.Value1() + weight.Value2());
	}

	{
		Lattice lat;
		Posterior post;
		double total_like = 0.0, lat_like;
		double total_ac_like = 0.0, lat_ac_like; // acoustic likelihood weighted by posterior.
		
		if (!decoder.GetRawLattice(&lat)) 
			KALDI_ERR << "Unexpected problem getting lattice for utterance " << utt;
		fst::Connect(&lat); // Will get rid of this later... shouldn't have any
		kaldi::uint64 props = lat.Properties(fst::kFstProperties, false);
		if (!(props & fst::kTopSorted)) {
			if (fst::TopSort(&lat) == false)
				KALDI_ERR << "Cycles detected in lattice.";
		}

		lat_like = kaldi::LatticeForwardBackward(lat, &post, &lat_ac_like);
		posterior_writer->Write(utt, post);
	}

	KALDI_LOG << "Log-like per frame for utterance " << utt << " is "
		<< (likelihood / num_frames) << " over "
		<< num_frames << " frames.";
	KALDI_VLOG(2) << "Cost for utterance " << utt << " is "
		<< weight.Value1() << " + " << weight.Value2();
	*like_ptr = likelihood;
	return true;
}

int main(int argc, char *argv[]) {
	try {
		using namespace kaldi;
		typedef kaldi::int32 int32;
		using fst::SymbolTable;
		using fst::VectorFst;
		using fst::StdArc;

		const char *usage =
			"Generate lattices, reading log-likelihoods as matrices\n"
			" (model is needed only for the integer mappings in its transition-model)\n"
			"Usage: latgen-faster-mapped [options] trans-model-in (fst-in|fsts-rspecifier) loglikes-rspecifier"
			" lattice-wspecifier [ words-wspecifier [alignments-wspecifier] ]\n";
		ParseOptions po(usage);

		bool allow_partial = false, silent = false;
		BaseFloat acoustic_scale = 0.1;
		LatticeFasterDecoderConfig config;
		std::string word_syms_filename;
		NnetForwardOptions nnet_opts;

		config.Register(&po);
		nnet_opts.Register(&po);
		po.Register("acoustic-scale", &acoustic_scale, "Scaling factor for acoustic likelihoods");
		po.Register("silent", &silent, "Don't print any messages");
		po.Register("word-symbol-table", &word_syms_filename, "Symbol table for words [for debug output]");
		po.Register("allow-partial", &allow_partial, "If true, produce output even if end state was not reached.");

		po.Read(argc, argv);

		if (po.NumArgs() < 4 || po.NumArgs() > 7) {
			po.PrintUsage();
			exit(1);
		}

		std::string model_in_filename = po.GetArg(1),
			fst_in_str = po.GetArg(2),
			feature_rspecifier = po.GetArg(3),
			words_wspecifier = po.GetOptArg(4),
			alignment_wspecifier = po.GetOptArg(5),
			lattice_wspecifier = po.GetArg(6),
			posteriors_wspecifier = po.GetArg(7);

#if HAVE_CUDA==1
		if(use_gpu_id > -2)
			CuDevice::Instantiate().SelectGpuId(use_gpu_id);
#endif
		
		/// load transition model;
		TransitionModel trans_model;
		ReadKaldiObject(model_in_filename, &trans_model);

		/// load word symbols;
		fst::SymbolTable *word_syms = NULL;
		if (word_syms_filename != "") {
			if (!(word_syms = fst::SymbolTable::ReadText(word_syms_filename)))
				KALDI_ERR << "Could not read symbol table from file "
				<< word_syms_filename;
		}

		/// Loading wfst;
		VectorFst<StdArc> *decode_fst = NULL;
		{
			std::ifstream is(fst_in_str.c_str(), std::ifstream::binary);
			if (!is.good()) KALDI_ERR << "Could not open decoding-graph FST "
				<< fst_in_str;
			decode_fst =
				VectorFst<StdArc>::Read(is, fst::FstReadOptions(fst_in_str));
			if (decode_fst == NULL) // fst code will warn.
				exit(1);
		}

		//// Initilize for nnet forward;
		Nnet nnet_transf, nnet;
		CuVector<BaseFloat> priors;
		if (!nnet_opts.loglikes_in) {
			InitNnet(nnet_opts, nnet_transf, nnet, priors);
		}

		/// Output;
		Int32VectorWriter words_writer(words_wspecifier);
		Int32VectorWriter alignment_writer(alignment_wspecifier);
		kaldi::PosteriorWriter posterior_writer(posteriors_wspecifier);
		bool determinize = config.determinize_lattice;
		CompactLatticeWriter compact_lattice_writer;
		LatticeWriter lattice_writer;
		if (! (determinize ? compact_lattice_writer.Open(lattice_wspecifier)
			: lattice_writer.Open(lattice_wspecifier)))
			KALDI_ERR << "Could not open table for writing lattices: "
			<< lattice_wspecifier;

		double tot_like = 0.0;
		kaldi::int64 frame_count = 0;
		int num_success = 0, num_fail = 0;
		Timer timer;
		LatticeFasterDecoder decoder(*decode_fst, config);
		
		if (nnet_opts.htk_in)
		{
			SequentialTableReader<HtkMatrixHolder> feature_reader(feature_rspecifier);
			for (; !feature_reader.Done(); feature_reader.Next()) {
				Timer timer2;
				std::string utt = feature_reader.Key();
				const Matrix<BaseFloat> &feature (feature_reader.Value().first);
				Matrix<BaseFloat> loglikes;
				if (!nnet_opts.loglikes_in) {
					NnetForward(nnet_opts, nnet_transf, nnet, priors, utt, feature, loglikes);
				} else {
					loglikes = feature;
				}


				if (loglikes.NumRows() == 0) {
					KALDI_WARN << "Zero-length utterance: " << utt;
					num_fail++;
					continue;
				}

				DecodableMatrixScaledMapped decodable(trans_model, loglikes, acoustic_scale);
				double like;
				
				if (GinkgoDecodeUtterance(
					decoder, decodable, word_syms, utt, acoustic_scale,
					determinize, allow_partial, &alignment_writer, &words_writer,
					&posterior_writer, &like)) {
						tot_like += like;
						frame_count += loglikes.NumRows();
						num_success++;
				} else num_fail++;

				if(!silent) KALDI_LOG << "real-time factor assuming 100 frames/sec is "
					<< (timer2.Elapsed()*100.0/loglikes.NumRows());
			}

		}

		delete decode_fst;
		

		double elapsed = timer.Elapsed();
		KALDI_LOG << "Time taken "<< elapsed
			<< "s: real-time factor assuming 100 frames/sec is "
			<< (elapsed*100.0/frame_count);
		KALDI_LOG << "Done " << num_success << " utterances, failed for "
			<< num_fail;
		KALDI_LOG << "Overall log-likelihood per frame is " << (tot_like/frame_count) << " over "
			<< frame_count<<" frames.";

		if (word_syms) delete word_syms;
		if (num_success != 0) return 0;
		else return 1;
	} catch(const std::exception &e) {
		std::cerr << e.what();
		return -1;
	}
}



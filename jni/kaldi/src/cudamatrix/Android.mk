include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/cudamatrix
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-cudamatrix
LOCAL_SRC_FILES :=  cuda-matrix-test.cc cu-device.cc cu-math.cc cu-matrix.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

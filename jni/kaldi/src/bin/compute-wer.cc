// bin/compute-wer.cc

// Copyright 2009-2011  Microsoft Corporation

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "util/parse-options.h"
#include "tree/context-dep.h"
#include "util/edit-distance.h"

int SplitWord2Char(const std::vector<std::string> &word_sent, std::vector<std::string> &char_sent)
{
	char_sent.clear();
	std::string word = "";
	std::string englishword = "";

	int i  = 0;
	std::vector<std::string>::const_iterator it_sent;
	for (it_sent = word_sent.begin(); it_sent != word_sent.end(); it_sent++) {
		i = 0;
		word = *it_sent;
		while (i < word.size()) {
			unsigned char ch =(unsigned char) word[i];
			if((ch >= 129 && ch <= 160) || (ch >= 170 && ch <= 254)) {//如果是汉字的第一个字节
				if (!englishword.empty()) {
					char_sent.push_back(englishword);
					englishword.clear();
				}
				if (i+1 < word.size()) {
					unsigned char next = (unsigned char) word[i+1];
					if (next != 127 && next >= 64 && next <= 254) {
						char_sent.push_back(word.substr(i, 2));
						i += 2;
					} else continue;
				}
			}
			else if (ch >= 161 && ch <= 169) {//如果是全角符号的第一个字节
				unsigned char next = (unsigned char) word[i+1];
				if (ch == 0xA3 && ((next >= 0xC1 && next <=0xDA) || (next >= 0xE1 && next <= 0xFA))){ //全角英文
					englishword += word.substr(i,2);
					i += 2;
				} else if (next != 127 && next >= 64 && next <= 254) {
					if (!englishword.empty()) {
						char_sent.push_back(englishword);
						englishword.clear();
					}
					char_sent.push_back(word.substr(i, 2));
					i += 2;
				} else continue;
			}
			else if ((ch < 127) && (ch > 32)) {//SBC printable characters
				if ((ch >= 41 && ch <= 90) || (ch >= 97 || ch <= 122)) {
					englishword += word.substr(i, 1);
					i++;
				} else {
					if (!englishword.empty()) {
						char_sent.push_back(englishword);
						englishword.clear();
					}

					char_sent.push_back(word.substr(i, 1));
					i++;
				}
			}
			else if ((ch <= 32) || (ch == 127)) {//SBC control characters
				if (!englishword.empty()) {
					char_sent.push_back(englishword);
					englishword.clear();
				}
				i++;
			}
			else {
				if (!englishword.empty()) {
					char_sent.push_back(englishword);
					englishword.clear();
				}
				i += 2;
			}
		}
		if (!englishword.empty()) {
			char_sent.push_back(englishword);
			englishword.clear();
		}
	}

	return static_cast<int>(char_sent.size());
}


int main(int argc, char *argv[]) {
	using namespace kaldi;
	typedef kaldi::int32 int32;

	try {
		const char *usage =
			"Compute WER by comparing different transcriptions\n"
			"Takes two transcription files, in kaldi integer format\n"
			"Usage: compute-wer [options] <ref-rspecifier> <hyp-rspecifier>\n";
		ParseOptions po(usage);

		std::string mode = "strict";
		bool text_input = false;  //  if this is true, we expect symbols as strings,
		bool cal_percent = false;

		po.Register("mode", &mode,
			"Scoring mode: \"present\"|\"all\"|\"strict\":\n"
			"  \"present\" means score those we have transcriptions for\n"
			"  \"all\" means treat absent transcriptions as empty\n"
			"  \"strict\" means die if all in ref not also in hyp");
		po.Register("text", &text_input, "Expect strings, not integers, as input.");
		po.Register("cal-percent", &cal_percent, "Expect strings, not integers, as input.");

		po.Read(argc, argv);

		if (po.NumArgs() != 2) {
			po.PrintUsage();
			exit(1);
		}

		std::string ref_rspecifier = po.GetArg(1);
		std::string hyp_rspecifier = po.GetArg(2);

		if (mode != "strict"
			&& mode != "present"
			&& mode != "all") {
				KALDI_ERR << "--mode option invalid: expected \"present\"|\"all\"|\"strict\", got "<<mode;
		}



		int32 num_words = 0, word_errs = 0, num_sent = 0, sent_errs = 0,
			num_ins = 0, num_del = 0, num_sub = 0, num_absent_sents = 0;

		if (!text_input) {

			SequentialInt32VectorReader ref_reader(ref_rspecifier);
			RandomAccessInt32VectorReader hyp_reader(hyp_rspecifier);

			for (; !ref_reader.Done(); ref_reader.Next()) {
				std::string key = ref_reader.Key();
				const std::vector<int32> &ref_sent = ref_reader.Value();
				std::vector<int32> hyp_sent;
				if (!hyp_reader.HasKey(key)) {
					if (mode == "strict")
						KALDI_ERR << "No hypothesis for key " << key << " and strict "
						"mode specifier.";
					num_absent_sents++;
					if (mode == "present") // do not score this one.
						continue;
				} else {
					hyp_sent = hyp_reader.Value(key);
				}
				num_words += ref_sent.size();
				int32 ins, del, sub;
				word_errs += LevenshteinEditDistance(ref_sent, hyp_sent, &ins, &del, &sub);
				num_ins += ins; num_del += del; num_sub += sub;

				num_sent++;
				sent_errs += (ref_sent != hyp_sent);
			}
		} else {
			SequentialTokenVectorReader ref_reader(ref_rspecifier);
			RandomAccessTokenVectorReader hyp_reader(hyp_rspecifier);

			for (; !ref_reader.Done(); ref_reader.Next()) {
				std::string key = ref_reader.Key();
				const std::vector<std::string> &ref_sent0 = ref_reader.Value();
				std::vector<std::string> hyp_sent0;
				if (!hyp_reader.HasKey(key)) {
					if (mode == "strict")
						KALDI_ERR << "No hypothesis for key " << key << " and strict "
						"mode specifier.";
					num_absent_sents++;
					if (mode == "present") // do not score this one.
						continue;
				} else {
					hyp_sent0 = hyp_reader.Value(key);
				}
				std::vector<std::string> ref_sent, hyp_sent;
				SplitWord2Char(ref_sent0, ref_sent);
				SplitWord2Char(hyp_sent0, hyp_sent);

				num_words += ref_sent.size();
				int32 ins, del, sub;
				word_errs += LevenshteinEditDistance(ref_sent, hyp_sent, &ins, &del, &sub);
				num_ins += ins; num_del += del; num_sub += sub;

				num_sent++;
				sent_errs += (ref_sent != hyp_sent);
			}
		}

		BaseFloat percent_wer = 100.0 * static_cast<BaseFloat>(word_errs)
			/ static_cast<BaseFloat>(num_words);
		std::cout.precision(2);
		std::cerr.precision(2);
		std::cout << "%WER "<<std::fixed<<percent_wer<< " [ "<<word_errs<<" / "<<num_words
			<<", "<<num_ins<<" ins, "<<num_del<<" del, "<<num_sub<<" sub ]"
			<< (num_absent_sents !=  0 ? " [PARTIAL]" : "") << '\n';
		if (cal_percent) {
			BaseFloat percent_corr = 100.0 * (1.0 - static_cast<BaseFloat>(word_errs - num_ins)
				/ static_cast<BaseFloat>(num_words));
			BaseFloat percent_acc = 100.0 - percent_wer;
			BaseFloat percent_ins = 100.0 * static_cast<BaseFloat>(num_ins) / static_cast<BaseFloat>(num_words);
			BaseFloat percent_del = 100.0 * static_cast<BaseFloat>(num_del) / static_cast<BaseFloat>(num_words);
			BaseFloat percent_sub = 100.0 * static_cast<BaseFloat>(num_sub) / static_cast<BaseFloat>(num_words);
			std::cout << "%Corr "<<std::fixed<<percent_corr<<", %Acc "<< percent_acc 
				<< ", %Ins " << percent_ins << ", %Del " << percent_del << ", %Sub " << percent_sub << "\n";
		}
		BaseFloat percent_ser = 100.0 * static_cast<BaseFloat>(sent_errs)
			/ static_cast<BaseFloat>(num_sent);
		std::cout << "%SER "<<std::fixed<<percent_ser<< " [ "
			<<sent_errs<<" / "<<num_sent<<" ]\n";
		std::cout << "Scored " << num_sent << " sentences, "
			<< num_absent_sents << " not present in hyp.\n";
		return 0;
	} catch (const std::exception &e) {
		std::cerr << e.what();
		return -1;
	}
}



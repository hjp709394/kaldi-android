include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/fstext
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-fstext
LOCAL_SRC_FILES :=  context-fst-test.cc deterministic-fst-test.cc determinize-lattice-pruned-test.cc determinize-lattice-test.cc determinize-star-test.cc factor-test.cc fstext-utils-test.cc lattice-utils-test.cc lattice-weight-test.cc pre-determinize-test.cc push-special.cc push-special-test.cc remove-eps-local-test.cc rescale-test.cc table-matcher-test.cc trivial-factor-weight-test.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  kaldi-tree  fst
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

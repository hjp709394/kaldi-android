include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/thread
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-thread
LOCAL_SRC_FILES :=  kaldi-barrier.cc kaldi-mutex.cc kaldi-semaphore.cc kaldi-task-sequence-test.cc kaldi-thread.cc kaldi-thread-test.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-util  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/feat
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-feat
LOCAL_SRC_FILES :=  feature-fbank.cc feature-fbank-test.cc feature-functions.cc feature-mfcc.cc feature-mfcc-test.cc feature-plp.cc feature-plp-test.cc feature-spectrogram.cc mel-computations.cc wave-reader.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-matrix  kaldi-util  kaldi-gmm  kaldi-transform  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

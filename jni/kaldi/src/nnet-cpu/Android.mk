include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/nnet-cpu
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-nnet-cpu
LOCAL_SRC_FILES :=  am-nnet.cc combine-nnet.cc combine-nnet-a.cc combine-nnet-fast.cc mixup-nnet.cc nnet-component.cc nnet-component-test.cc nnet-compute.cc nnet-fix.cc nnet-functions.cc nnet-lbfgs.cc nnet-limit-rank.cc nnet-nnet.cc nnet-precondition.cc nnet-precondition-test.cc nnet-randomize.cc nnet-stats.cc nnet-update.cc nnet-update-parallel.cc rescale-nnet.cc shrink-nnet.cc train-nnet.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  kaldi-thread  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

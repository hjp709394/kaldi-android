include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/matrix
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-matrix
LOCAL_SRC_FILES :=  compressed-matrix.cc kaldi-gpsr.cc kaldi-gpsr-test.cc kaldi-matrix.cc kaldi-vector.cc matrix-functions.cc matrix-lib-test.cc optimization.cc packed-matrix.cc qr.cc sp-matrix.cc srfft.cc tp-matrix.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

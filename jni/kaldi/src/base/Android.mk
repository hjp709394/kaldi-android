include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/base
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-base
LOCAL_SRC_FILES :=  io-funcs.cc io-funcs-test.cc kaldi-error.cc kaldi-error-test.cc kaldi-math.cc kaldi-math-test.cc kaldi-utils.cc
LOCAL_STATIC_LIBRARIES :=  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

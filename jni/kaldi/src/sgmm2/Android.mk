include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/sgmm2
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-sgmm2
LOCAL_SRC_FILES :=  am-sgmm.cc am-sgmm-project.cc am-sgmm-test.cc estimate-am-sgmm.cc estimate-am-sgmm-ebw.cc estimate-am-sgmm-multi.cc estimate-am-sgmm-test.cc fmllr-sgmm.cc fmllr-sgmm-test.cc sgmm-clusterable.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  kaldi-gmm  kaldi-tree  kaldi-transform  kaldi-thread  kaldi-hmm  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

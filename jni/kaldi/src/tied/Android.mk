include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/tied
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-tied
LOCAL_SRC_FILES :=  am-tied-diag-gmm.cc am-tied-diag-gmm-test.cc am-tied-full-gmm.cc am-tied-full-gmm-test.cc mle-am-tied-diag-gmm.cc mle-am-tied-full-gmm.cc mle-tied-gmm.cc tied-gmm.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  kaldi-gmm  kaldi-tree  kaldi-transform  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

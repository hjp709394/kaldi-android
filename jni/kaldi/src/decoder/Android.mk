include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/decoder
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-decoder
LOCAL_SRC_FILES :=  decodable-am-diag-gmm.cc decodable-am-sgmm.cc decodable-am-sgmm2.cc decodable-am-tied-diag-gmm.cc decodable-am-tied-full-gmm.cc faster-decoder.cc lattice-faster-decoder.cc lattice-simple-decoder.cc lattice-tracking-decoder.cc training-graph-compiler.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  kaldi-gmm  kaldi-sgmm  kaldi-hmm  kaldi-tree  kaldi-transform  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

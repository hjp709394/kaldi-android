include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/tree
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-tree
LOCAL_SRC_FILES :=  build-tree.cc build-tree-questions.cc build-tree-test.cc build-tree-utils.cc build-tree-utils-test.cc clusterable-classes.cc cluster-utils.cc cluster-utils-test.cc context-dep.cc context-dep-test.cc event-map.cc event-map-test.cc tree-renderer.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-matrix  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

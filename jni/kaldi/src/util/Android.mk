include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/util
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-util
LOCAL_SRC_FILES :=  const-integer-set-test.cc edit-distance-test.cc hash-list-test.cc kaldi-io.cc kaldi-io-test.cc kaldi-table.cc kaldi-table-test.cc parse-options.cc parse-options-test.cc simple-io-funcs.cc stl-utils-test.cc text-utils.cc text-utils-test.cc timer-test.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-matrix  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

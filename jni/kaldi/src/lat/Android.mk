include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/kaldi/src/lat
LOCAL_C_INCLUDES += $(TOP_PATH)/kaldi/src $(TOP_PATH)/openfst/src/include/
LOCAL_MODULE    := kaldi-lat
LOCAL_SRC_FILES :=  kaldi-lattice.cc kaldi-lattice-test.cc kws-functions.cc lattice-functions.cc phone-align-lattice.cc sausages.cc word-align-lattice.cc
LOCAL_STATIC_LIBRARIES :=  kaldi-base  kaldi-util  kaldi-hmm  
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib
include $(BUILD_STATIC_LIBRARY)

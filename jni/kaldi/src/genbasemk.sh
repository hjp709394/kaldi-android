#! /bin/sh

r_path="kaldi/src"

#dependencies
base_d=""
matrix_d="base"
util_d="base matrix"
thread_d="util"
feat_d="base matrix util gmm transform"
tree_d="base util matrix"
optimization_d="base matrix"
gmm_d="base util matrix tree"
tied_d="base util matrix gmm tree transform"
transform_d="base util matrix gmm tree"
sgmm_d="base util matrix gmm tree transform thread hmm"
sgmm2_d="base util matrix gmm tree transform thread hmm"
fstext_d="base util matrix tree"
hmm_d="base tree matrix"
lm_d="base util"
decoder_d="base util matrix gmm sgmm hmm tree transform"
lat_d="base util hmm"
cudamatrix_d="base util matrix"
nnet_d="base util matrix cudamatrix"
nnet_cpu_d="base util matrix thread"


# generating
for file in `ls` 
do
	if [ -d $file ] 
	then
		if [ -f $file/Android.mk ]
		then
			rm $file/Android.mk
		fi
		f=`ls $file | grep -E "\.(cpp|cc|c|cxx)$"`
		filelist=""
		for t in $f
		do
			filelist+="$t "
		done
		echo "include \$(CLEAR_VARS)" > $file/Android.mk
		echo "LOCAL_PATH := \$(TOP_PATH)/$r_path/$file" >> $file/Android.mk
		echo "LOCAL_C_INCLUDES += \$(TOP_PATH)/$r_path" >> $file/Android.mk
		echo "LOCAL_MODULE    := kaldi-$file" >> $file/Android.mk
		echo "LOCAL_SRC_FILES := $filelist" >> $file/Android.mk
		echo "LOCAL_STATIC_LIBRARIES := " >> $file/Android.mk
		echo "LOCAL_LDLIBS := -L\$(SYSROOT)/usr/lib" >> $file/Android.mk
		echo "include \$(BUILD_STATIC_LIBRARY)" >> $file/Android.mk
	fi
done

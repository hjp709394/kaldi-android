package org.shrc.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DecoderBootBroadcastReceiver extends BroadcastReceiver {
	
	public void onReceive(Context context, Intent intent) {
		  if("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
		     Intent serviceLauncher = new Intent(context, DecoderService.class);
		     context.startService(serviceLauncher);
		  }
	}
}
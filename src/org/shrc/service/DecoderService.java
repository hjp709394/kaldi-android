package org.shrc.service;

import org.shrc.nativeinterface.NativeInterface;
import org.shrc.util.AppLog;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;

public class DecoderService extends Service {

	private DecoderServiceBinder mBinder = new DecoderServiceBinder();

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		NativeInterface.doInit();
	}
//
//	@Override
//	public void onStart(Intent intent, int startId) {
//		super.onStart(intent, startId);
//	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		NativeInterface.doRelease();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}
//	
//	public String getRecognitionResult() {
//		return NativeInterface.recognize();
//	}
	
	//HJP:BUG
	//HTK can only work in single-thread mode,
	//so this binder is not thread-safe
	public class DecoderServiceBinder extends Binder {    
//	    public VoiceDialService getService() {  
//	        return VoiceDialService.this;
//	    }
	    
		public final static int CODE_DECODING = FIRST_CALL_TRANSACTION;
		
		@Override
	    public boolean onTransact( int code , Parcel data , Parcel reply , int flags ) {
	        //called when client calls transact on returned Binder
//			AppLog.ilog("DecoderService.onTransact");
			if (code == CODE_DECODING) {
		    	String result = NativeInterface.doDecode();
		    	reply.writeString(result);
			}
			return true;
	    }
	}
}

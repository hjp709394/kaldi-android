package org.shrc.util;

public class Globals {
	public static final String APP_NAME = "SHRC Wave2Text";
	public static final String DEF_WAV_FN = "voice.wav";
	public static final String TRANS_PROMPT = "[Transcrip Result]";
}

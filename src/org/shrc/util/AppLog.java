package org.shrc.util;

import android.util.Log;

public class AppLog {
	private static final String APP_TAG = Globals.APP_NAME;

	public static void ilog(String message) {
		Log.i(APP_TAG, message);
	}
	
	public static void elog(String message) {
		Log.e(APP_TAG, message);
	}
}
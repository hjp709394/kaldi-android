package org.shrc.util;

import java.util.LinkedList;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

public class Recorder {
	
	public interface FinishedCallBack {
		public void callBack();
	}
	
	public interface SamplesCallBack {
		public void callBack(byte[] samples);
	}
	
	private static final SamplesCallBack EMPTY_SAMPLES_CALL_BACK = new SamplesCallBack() {
		@Override
		public void callBack(byte[] samples) {
		}
	};
	
	private static final FinishedCallBack EMPTY_FINISHED_CALL_BACK = new FinishedCallBack() {
		@Override
		public void callBack() {
		}
	};
	
	private FinishedCallBack finishedCallBack = EMPTY_FINISHED_CALL_BACK;
	
	private SamplesCallBack newSamplesCallBack = EMPTY_SAMPLES_CALL_BACK;
		
	//private Socket mSocket = null;
	
	public void setFinishedCallBack(FinishedCallBack cb)
	{
		finishedCallBack = cb;
	}
	
	public void setNewSamplesCallBack(SamplesCallBack cb)
	{
		newSamplesCallBack = cb;
	}
	
	public void start()
	{
		mStopRecording = false;
		mIsRecording = true;
		mRecordBuffer.clear();
		new Thread(readRecordingDataTask).start();
	}
	
	public LinkedList<byte[]> getBuffer() {
		return mRecordBuffer;
	}
	
	boolean mStopRecording = false;
	boolean mIsRecording = false;
	
	public void stop() {
		while (mIsRecording) {
			mStopRecording = true;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private AudioRecord mRecorder = null;
	private LinkedList<byte[]> mRecordBuffer  = new LinkedList<byte[]>();
	public static final int SAMPLERATE = 8000;	//HJP:TODO:Make it configurable
	private static final int CHANNELS = AudioFormat.CHANNEL_IN_MONO;	//HJP:TODO:Make it configurable
	private static final int AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;	//HJP:TODO:Make it configurable
	
	public static final int CHANNEL_CNT = getChannelCntFromAudioFormat(CHANNELS);	//HJP:TODO:Make it configurable
	public static final int BITS_PER_SAMPLE = getBitsPerSampleFromAudioFormat(CHANNELS, AUDIO_ENCODING);	//HJP:TODO:Make it configurable
	
	public final static int getChannelCntFromAudioFormat(int channels) {
		switch (channels) {
		case AudioFormat.CHANNEL_IN_MONO: case AudioFormat.CHANNEL_IN_LEFT: case AudioFormat.CHANNEL_IN_RIGHT:
			return 1;
		case AudioFormat.CHANNEL_IN_STEREO:
			return 2;
		default:
			throw new IllegalArgumentException("AudioFormat.CHANNEL_IN_XXX " + channels + " is not valid");
		}
	}
	
	public final static int getBitsPerSampleFromAudioFormat(int channels, int encoding) {
		int q, c;
		switch (encoding) {
		case AudioFormat.ENCODING_PCM_8BIT:
			q = 8;
			break;
		case AudioFormat.ENCODING_PCM_16BIT:
			q=16;
			break;
		default:
			throw new IllegalArgumentException("AudioFormat.ENCODING_XXX " + encoding + " is not valid");
		}
		c = getChannelCntFromAudioFormat(channels);
		return q * c;
	}
	
	public static final int BUFFER_SIZE = AudioRecord.getMinBufferSize(SAMPLERATE,
			CHANNELS, AUDIO_ENCODING) * 2;	//HJP:TODO:Make it configurable, should be updated whenever the parameters change 
	
	private Runnable readRecordingDataTask = new Runnable() {
		byte[] tmpBuffer;
		@Override
		public void run() {
			//AppLog.log("buffer size: " + BUFFER_SIZE);
			
			//If not use Voice_Recognition source, 
			//the input would use Automatic Gain Control which would decrease the performance
			mRecorder = new AudioRecord(MediaRecorder.AudioSource.VOICE_RECOGNITION,
					SAMPLERATE, CHANNELS,
					AUDIO_ENCODING, BUFFER_SIZE);
			mRecorder.startRecording();
			
			mIsRecording = true;
			int sizeRead = 0;
			while (!mStopRecording) {
				tmpBuffer = new byte[BUFFER_SIZE];
				sizeRead = 0;
				while (sizeRead < BUFFER_SIZE) {
					//HJP
					//I am not sure whether mRecorder would read less bytes than BUFFER_SIZE
					int ret = mRecorder.read(tmpBuffer, sizeRead, BUFFER_SIZE - sizeRead);
					if (ret < 0) {
						mStopRecording = true;
						break;
					}
					sizeRead += ret;
				}
				if (sizeRead > 0) {
					for (int i = sizeRead; i < BUFFER_SIZE; ++i) {
						tmpBuffer[i] = 0;
					}
					mRecordBuffer.addLast(tmpBuffer);
					
					//CallBack should not be time consuming
					newSamplesCallBack.callBack(tmpBuffer);
				}
			}
			mRecorder.stop();
			mRecorder.release();
			mIsRecording = false;
			mRecorder = null;
			
			finishedCallBack.callBack();
		}
	};	
}

package org.shrc.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import android.util.Log;

public class WaveSaver {
	
	public static void writeWave(
			String baseDirectionString, 
			String fn,
			int sampleRate,
			int bitsPerSample,
			int channelsCnt,
			LinkedList<byte[]> linkedBuffer) {
		if (linkedBuffer != null && linkedBuffer.isEmpty()) {
			return;
		}

		File baseDirection = new File(baseDirectionString);
		try {
			if (!baseDirection.exists()) {
				baseDirection.mkdirs();
			}
			FileOutputStream fos = new FileOutputStream(
					baseDirection.getAbsolutePath() + '/' + fn, 
					false);
			Log.v("WaveSaver", baseDirection.getAbsolutePath());
			writeWaveFileHeader(
					fos, 
					linkedBuffer.getFirst().length * linkedBuffer.size(), 
					sampleRate, 
					bitsPerSample,
					channelsCnt);
			Iterator<byte[]> ite = linkedBuffer.iterator();
			while (ite.hasNext()) {
				byte[] buf = ite.next();
				fos.write(buf);
			}
			//linkedBuffer.clear();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void writeWaveFileHeader(
			FileOutputStream out, 
			long totalAudioLen,
			long longSampleRate, 
			int bitsPerSample,
			int channels)
					throws IOException {
		long totalDataLen = totalAudioLen + 36;
		long byteRate = bitsPerSample * longSampleRate * channels / 8;
		byte[] header = new byte[] {
				'R', 'I', 'F', 'F',
				(byte)(totalDataLen & 0xff), (byte)((totalDataLen >> 8) & 0xff), (byte) ((totalDataLen >> 16) & 0xff), (byte)((totalDataLen >> 24) & 0xff),
				'W', 'A', 'V', 'E',
				'f', 'm', 't', ' ',
				16, 0, 0, 0,
				1, 0,
				(byte) channels, 0,
				(byte)(longSampleRate & 0xff), (byte)((longSampleRate >> 8) & 0xff), (byte) ((longSampleRate >> 16) & 0xff), (byte) ((longSampleRate >> 24) & 0xff),
				(byte) (byteRate & 0xff), (byte)((byteRate >> 8) & 0xff), (byte) ((byteRate >> 16) & 0xff), (byte) ((byteRate >> 24) & 0xff),
				(byte) (channels * bitsPerSample / 8), // block align
				0,
				(byte)bitsPerSample, // bits per sample
				0,
				'd', 'a', 't', 'a',
				(byte)(totalAudioLen & 0xff), (byte)((totalAudioLen >> 8) & 0xff), (byte)((totalAudioLen >> 16) & 0xff), (byte)((totalAudioLen >> 24) & 0xff)		
				};
//
//		header[0] = 'R'; header[1] = 'I'; header[2] = 'F'; header[3] = 'F';// RIFF/WAVE header
//		header[4] = (byte) (totalDataLen & 0xff);		header[5] = (byte) ((totalDataLen >> 8) & 0xff);		header[6] = (byte) ((totalDataLen >> 16) & 0xff);		header[7] = (byte) ((totalDataLen >> 24) & 0xff);
//		header[8] = 'W'; 		header[9] = 'A';		header[10] = 'V';		header[11] = 'E';
//		header[12] = 'f';		header[13] = 'm';		header[14] = 't';		header[15] = ' '; // 'fmt ' chunk
//		header[16] = 16; 		header[17] = 0;		header[18] = 0;		header[19] = 0;// 4 bytes: size of 'fmt ' chunk
//		header[20] = 1;		header[21] = 0; // format = 1
//		header[22] = (byte) channels;
//		header[23] = 0;
//		header[24] = (byte) (longSampleRate & 0xff); 		header[25] = (byte) ((longSampleRate >> 8) & 0xff);		header[26] = (byte) ((longSampleRate >> 16) & 0xff);		header[27] = (byte) ((longSampleRate >> 24) & 0xff);
//		header[28] = (byte) (byteRate & 0xff); 		header[29] = (byte) ((byteRate >> 8) & 0xff);		header[30] = (byte) ((byteRate >> 16) & 0xff);		header[31] = (byte) ((byteRate >> 24) & 0xff);
//		header[32] = (byte) (2 * 16 / 8); // block align
//		header[33] = 0;
//		header[34] = (byte)bitsPerSample; // bits per sample
//		header[35] = 0;
//		header[36] = 'd'; 		header[37] = 'a';		header[38] = 't';		header[39] = 'a';
//		header[40] = (byte) (totalAudioLen & 0xff);		header[41] = (byte) ((totalAudioLen >> 8) & 0xff);		header[42] = (byte) ((totalAudioLen >> 16) & 0xff);		header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

		out.write(header, 0, 44);
	}
	
}

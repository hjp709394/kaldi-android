package org.shrc.activity;

import org.shrc.R;
import org.shrc.service.DecoderService;
import org.shrc.service.DecoderService.DecoderServiceBinder;
import org.shrc.util.AppLog;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.widget.ProgressBar;

public class DecodingProgressActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.decoding_progress);
		
		connectDecoderService();
//		new Thread(new DecodingTask()).start();
	}
	
	private ServiceConnection mServiceConnection = new DecoderServiceConnection();
	
	private DecoderServiceBinder mServiceBinder = null;

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(mServiceConnection);
		mServiceConnection = null;
	};
	
	private void connectDecoderService() {
		Intent serviceIntent = new Intent().setClass(DecodingProgressActivity.this,
				DecoderService.class);
		startService(serviceIntent);
		//HJP:BUG:Bind failure undetected
		bindService(serviceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}
	
	private class DecoderServiceConnection implements ServiceConnection {
		@Override
		public void onServiceConnected(ComponentName inName,
				IBinder serviceBinder) {
//			AppLog.ilog(inName.getShortClassName());
			if (inName.getShortClassName().endsWith("DecoderService")) {
				mServiceBinder = ((DecoderService.DecoderServiceBinder) serviceBinder);
				// mTracker = mService.mConfiguration.mTracker;
			}
			new Thread(new DecodingTask()).start();	// start decoding task
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
//			String message = "Decoder Service Killed";
//			new AlertDialog.Builder(DecoderActivity.this).setMessage(message)
//					.show();
//			connectDecoderService();
		}
	}
	
	private class DecodingTask implements Runnable {
		@Override
		public void run() {
			isDecodingDone = false;
			new Thread(new UpdateProgressBarTask()).start();

			Parcel p = Parcel.obtain();
			Parcel result = Parcel.obtain();
			try {
				mServiceBinder.transact(DecoderServiceBinder.CODE_DECODING, p,
						result, 0);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			String resultString = result.readString();
			AppLog.ilog("[DecodingProgressActivity.DecodeTask] result: " + resultString);
			p.recycle();
			result.recycle();
			// String resultString = "EMPTY";
			//
			// try {
			// Thread.sleep(3000);
			// } catch (InterruptedException e) {
			// e.printStackTrace();
			// }

			isDecodingDone = true;

			Intent intent = new Intent();
			intent.setClass(DecodingProgressActivity.this, MainActivity.class);
			intent.putExtra(DECODING_RESULT_KEY, resultString);
			setResult(Activity.RESULT_OK, intent);
			DecodingProgressActivity.this.finish();
		}
	};

	public static String DECODING_RESULT_KEY = "RESULT";
	
	private boolean isDecodingDone = true;
	
	private class UpdateProgressBarTask implements Runnable {
		private final int MAX_PROGRESS = 100;
		private final int UPDATE_INTERVAL = 100;
		@Override
		public void run() {
			ProgressBar pb = (ProgressBar)findViewById(R.id.decodingProgress);
			pb.setMax(MAX_PROGRESS);
			int i = 0;
			while (!isDecodingDone) {
				pb.setProgress(i++);
				if (i == MAX_PROGRESS) {
					i = 0;
				}
				try {
					Thread.sleep(UPDATE_INTERVAL);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			// TEST if this thread not finshed before decoding thread exit
//			try {
//				Thread.sleep(100000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//			AppLog.ilog("Update Thread exiting");
		}
	};
}

package org.shrc.activity;

import org.shrc.R;
import org.shrc.nativeinterface.NativeInterface;
import org.shrc.util.Globals;
import org.shrc.util.Recorder;
import org.shrc.util.WaveSaver;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		TextView transTextView = (TextView) findViewById(R.id.transTextView);
		transTextView.setMovementMethod(new ScrollingMovementMethod());
		
		amplitudeBar = (ProgressBar)findViewById(R.id.amplitude);
		amplitudeBar.setIndeterminate(false);
		amplitudeBar.setMax(1 << Recorder.BITS_PER_SAMPLE);
		
		setUpListeners();
		setUpRecorderCallBack();
		
		//connectDecoderService();
		
		NativeInterface.doTest();
	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
		
//	protected void onDestroy() {
//		super.onDestroy();
//		unbindService(mServiceConnection);
//		mServiceConnection = null;
//	};

	//Intent serviceIntent = null;

//	private void connectDecoderService() {
//		Intent serviceIntent = new Intent().setClass(MainActivity.this,
//				DecoderService.class);
//		startService(serviceIntent);
//		//HJP:BUG
//		//Bind failure undetected
//		bindService(serviceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
//	}

	private void setUpListeners() {
		Button btn = (Button) findViewById(R.id.buttonSpeak);
		btn.setOnClickListener(new OnSpeakListener());
		
		//btn = (Button) findViewById(R.id.buttonCall);
		//btn.setOnClickListener(onCallListener);
	}
	
//	private View.OnClickListener onCallListener = new View.OnClickListener() {
//		@Override
//		public void onClick(View v) {
//			TextView tv = (TextView)findViewById(R.id.phoneText);
//			String phone = tv.getText().toString();
//			if (ContactUtil.isPhoneNumber(phone.trim())) {
//				Intent myIntentDial = new Intent(Intent.ACTION_CALL,
//						Uri.parse("tel:" + phone));
//				myIntentDial.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(myIntentDial);
//			}
//		}
//	};

	//private ServiceConnection mServiceConnection = new DecoderServiceConnection();

	Recorder mRecorder = new Recorder();
	
	//WaveView wv;
	ProgressBar amplitudeBar;
	
	private class UpdateAmplitudeCallBack implements Recorder.SamplesCallBack {
		@Override
		public void callBack(final byte[] samples) {
//			wv.addSample(samples);
//			runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//					int progress = 0;
//					for (int i = 0; i < samples.length; i += 2) {	// save time: offset
//						progress = Math.max(	// HJP:NOTICE:Assumed 16bit
//								(samples[i] | (samples[i + 1] << 8)), 
//								progress);
//					}
//					amplitudeBar.setProgress(progress);
//				}
//			});
		}
	}
	
//	private class RecordingFinishedCallBack implements Recorder.FinishedCallBack {
//		@Override
//		public void callBack() {
//			String baseDirectionString = Environment.getExternalStorageDirectory().getPath() + '/' + Globals.APP_NAME;
//			String fn = Globals.DEF_WAV_FN;
//			WaveSaver.writeWave(
//					baseDirectionString, 
//					fn, 
//					Recorder.SAMPLERATE, 
//					Recorder.BITS_PER_SAMPLE, 
//					Recorder.CHANNEL_CNT,
//					mRecorder.getBuffer());
//			
////			//String seperator = ":";
////			final String result = NativeInterface.doRecognize().trim();
////			MainActivity.this.runOnUiThread(new Runnable() {
////				@Override
////				public void run() {
////					TextView n = (TextView)findViewById(R.id.transTextView);
////					n.setText(result);
////					
//////					Button button = (Button)findViewById(R.id.buttonSpeak);
//////					button.setText("Speak");
//////					state = SPEAK;
////				}
////			});
//		}
//	}
	
	private void setUpRecorderCallBack() {
		//TOUNCOMMENT
		mRecorder.setNewSamplesCallBack(new UpdateAmplitudeCallBack());
//		mRecorder.setFinishedCallBack(new RecordingFinishedCallBack());
	}
	
	// This Activity has 3 circular states: speak -> stop -> recognizing -> speak -> ...
//	private static final int SPEAK = 0;
//	private static final int STOP = 1;
//	private static final int RECOGNIZING = 2;
//	private int state = SPEAK;

	private static final int DECODING_PROGRESS_REQUESST_CODE = 0;
	
	private class OnSpeakListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			ToggleButton tb = (ToggleButton) v;
			if (tb.isChecked()) {
				mRecorder.start();
				TextView tv = (TextView)findViewById(R.id.transTextView);
				tv.setText(Globals.TRANS_PROMPT);
			} else {
				mRecorder.stop();
				WaveSaver.writeWave("/sdcard/shrc", "record.wav", mRecorder.SAMPLERATE, mRecorder.BITS_PER_SAMPLE, mRecorder.CHANNEL_CNT, mRecorder.getBuffer());
				//Pop up a progress bar and wait for the result
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, DecodingProgressActivity.class);
				v.setEnabled(false);
				startActivityForResult(intent, DECODING_PROGRESS_REQUESST_CODE);
			}
		}
	};
	
//	private View.OnClickListener doRecognitionListener = new OnClickListener() {
//		@Override
//		public void onClick(View v) {
//			try {
//				Parcel p = Parcel.obtain();
//				Parcel result = Parcel.obtain();
//				mServiceBinder.transact(DecoderServiceBinder.CODE_RECOGNIZE,
//						p, result, 0);
//				String resultString = result.readString();
//				AppLog.log("result: " + resultString);
//				p.recycle();
//				result.recycle();
//			} catch (RemoteException e) {
//				e.printStackTrace();
//			}
//		}
//	};

//	private DecoderServiceBinder mServiceBinder = null;

//	private class DecoderServiceConnection implements ServiceConnection {
//		@Override
//		public void onServiceConnected(ComponentName inName,
//				IBinder serviceBinder) {
//			if (inName.getShortClassName().endsWith("DecoderService")) {
//				mServiceBinder = ((DecoderService.DecoderServiceBinder) serviceBinder);
//				// mTracker = mService.mConfiguration.mTracker;
//			}
//		}
//
//		@Override
//		public void onServiceDisconnected(ComponentName name) {
////			String message = "Decoder Service Killed";
////			new AlertDialog.Builder(DecoderActivity.this).setMessage(message)
////					.show();
////			connectDecoderService();
//		}
//	}
	
//	private Activity progress = new Activity() {
//		private ProgressBar pb;
//		protected void onCreate(Bundle savedInstanceState) {
//			super.onCreate(savedInstanceState);
//			setContentView(R.layout.dial_prograss);
//			pb = (ProgressBar)findViewById(R.id.dialProgress);
//		};
//	};
	
	@Override	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		String result = "No Result";
		if (resultCode == Activity.RESULT_OK && requestCode == DECODING_PROGRESS_REQUESST_CODE) {
			result = data.getStringExtra(DecodingProgressActivity.DECODING_RESULT_KEY);
		}
		TextView tv = (TextView)findViewById(R.id.transTextView);
		tv.setText(result);
		ToggleButton tb = (ToggleButton)findViewById(R.id.buttonSpeak);
		tb.setEnabled(true);
	}

}

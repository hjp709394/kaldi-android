package org.shrc.nativeinterface;

import org.shrc.util.AppLog;

public class NativeInterface {

	static {
		try {
			System.loadLibrary("decoder");
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (UnsatisfiedLinkError e) {
			e.printStackTrace();
		}
	}

	// HJP:Current version is not thread safe
	// Lock when init or recognizing or release;
	private static final Object mMutex = new Object();

	private static boolean isInitialzed = false;

	public static void doInit() {
		synchronized (mMutex) {
			if (!isInitialzed) {
				init();
				isInitialzed = true;
			}
		}
	}

	private final static String DEF_RESULT = "[NOTHING]";

	public static String doDecode() {
		String res = DEF_RESULT;
		synchronized (mMutex) {
			if (!isInitialzed) {
				doInit();
			}
			res = decode();
		}
//		AppLog.ilog(res);
		return res;
	}

	public static void doRelease() {
		synchronized (mMutex) {
			if (!isInitialzed) {
				release();
			}
			isInitialzed = false;
		}
	}
	
	public static void doTest() {
		test("HELLO");
	}

	private static native String test(String msg);

//	private static native String init(String msg);
	private static native boolean init();

	private static native String decode();

	private static native boolean release();

}
